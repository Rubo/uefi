#!/bin/sh

set -e

# -DDEBUG
CFLAGS="-ggdb -ffreestanding -MMD -mno-red-zone -nostdlib -std=c2x -fno-stack-check -fno-stack-protector -mno-stack-arg-probe -pipe -Ibootloader -Ikernel"
BOOTLOADER_CFLAGS="-target x86_64-unknown-windows -fuse-ld=lld-link -Wl,-subsystem:efi_application -Wl,-entry:main"

mkdir -p build
rm -r build/*

clang $CFLAGS $BOOTLOADER_CFLAGS bootloader/*.c -o build/BootX64.efi

for f in kernel/*.c; do
    if [ $f = "kernel/irq.c" ]; then
        continue
    fi
    gcc $CFLAGS $f -c -o build/$(basename $f .c).o
done
gcc $CFLAGS -mgeneral-regs-only kernel/irq.c -c -o build/irq.o
nasm kernel/gdt.asm -f elf64 -o build/gdt.o
gcc $CFLAGS -T kernel/kernel.ld build/*.o -o build/kernel

BOOT=EFI/Boot
mkdir -p $BOOT
cp build/BootX64.efi $BOOT/BootX64.efi
cp build/kernel $BOOT/kernel
