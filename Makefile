# TODO: implement libssp functions and remove -fno-stack-*
CFLAGS := -ffreestanding -MMD -mno-red-zone -nostdlib -std=c2x -fno-stack-check -fno-stack-protector -mno-stack-arg-probe
ifeq ($(mode), debug)
	CFLAGS += -ggdb -DDEBUG
endif

BOOTLOADER_CFLAGS := -target x86_64-unknown-windows -fuse-ld=lld-link \
	-Wl,-subsystem:efi_application -Wl,-entry:main

SRCS := booloader.c kernel.c libc.c libc8.c

EFIDIR := ESP/EFI/Boot
OVMF_CODE := /usr/share/edk2-ovmf/x64/OVMF_CODE.fd

default: all

BootX64.efi: bootloader.c libc.c
	clang $(CFLAGS) $(BOOTLOADER_CFLAGS) $^ -o $@

%.o: %.c
	gcc $(CFLAGS) -c $< -o $@

%_asm.o: %.asm
	nasm $^ -f elf64 -o $@

interrupts.o: interrupts.c
	gcc $(CFLAGS) -mgeneral-regs-only -c $< -o $@

kernel: kernel.o libc8.o gdt_asm.o interrupts.o
	gcc $(CFLAGS) -mcmodel=kernel -T kernel.ld $^ -o $@

prepare: BootX64.efi kernel zap-light16.psf
	mkdir -p $(EFIDIR)
	cp $^ $(EFIDIR)

# - the q35 machine emulates PCIe, so that the ACPI MCFG table is present
# - OVMF is split in CODE and VARS, so that the read-only firmware executable can always be
#   up to date with the package manager, while the variable store is writeable
run:
	qemu-system-x86_64 \
		-machine q35,accel=kvm:tcg \
		-cpu qemu64 \
		-smp 4 \
		-device VGA,edid=on,xres=1280,yres=720 \
		-drive if=pflash,format=raw,readonly=on,file=$(OVMF_CODE) \
		-drive if=pflash,format=raw,file=OVMF_VARS.fd,cache=none \
		-drive format=raw,file=fat:rw:.,aio=native,cache.direct=on \
		-net none \
		-device qemu-xhci \
		-gdb tcp::1234

-include $(SRCS:.c=.d)

.PHONY: clean all default

all: BootX64.efi kernel

clean:
	rm -rf BootX64.efi *.o *.d *.lib ESP
