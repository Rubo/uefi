/* never trust the firmware */
#include "ELF.h"
#include "UEFI.h"
#include "bootinfo.h"
#include "libc.h"

#define ArrayCount(array) (sizeof(array) / sizeof(array[0]))
#define Print(str, ...) {                                          \
	CHAR16 msg[256];                                               \
	u16snprintf(msg, sizeof(msg), str, __VA_ARGS__);               \
	_SystemTable->ConOut->OutputString(_SystemTable->ConOut, msg); \
}
#define Error(msg, ...) {            \
	Print(msg, __VA_ARGS__);         \
	while (1) {                      \
		__asm__ __volatile__("hlt"); \
	}                                \
}
#define CompareGuids(a, b) (*((UINT128 *) a) == *((UINT128 *) b))

typedef void (__attribute__((sysv_abi)) *ENTRY)(struct boot_info *);

struct page_indices {
    UINT64 page_directory_pointer;
    UINT64 page_directory;
    UINT64 page_table;
    UINT64 page;
};

enum page_table_flag {
    PRESENT = 0,
    READ_WRITE = 1,
    USER_SUPER = 2,
    WRITE_THROUGH = 3,
    CACHE_DISABLED = 4,
    ACCESSED = 5,
    LARGER_PAGES = 7,
    CUSTOM0 = 9,
    CUSTOM1 = 10,
    CUSTOM2 = 11,
    NO_EXECUTE = 63, // supported only in some systems
};

struct page_table {
    UINT64 entries[512];
} __attribute__((aligned(PAGE_SIZE)));

EFI_SYSTEM_TABLE *_SystemTable;

void * RequestPage(void) {
	EFI_PHYSICAL_ADDRESS Memory;
	EFI_STATUS status = _SystemTable->BootServices->AllocatePages(AllocateAnyPages, EfiLoaderData, 1, &Memory);
	if (status) {
		Error("Return after AllocatePages\r\n");
	}
	return (void *) Memory;
}

void SetFlag(UINT64 *entry, enum page_table_flag flag, bool value) {
    *entry |= (UINT64) value << flag;
}

bool GetFlag(UINT64 *entry, enum page_table_flag flag) {
    bool value = *entry & (UINT64) 1 << flag;
    return value;
}

void SetAddress(UINT64 *entry, UINT64 address) {
    address &= 0x000000ffffffffff;
    *entry &= 0xfff0000000000fff;
    *entry |= address << 12;
}

#define get_address(entry) ((entry & 0x000ffffffffff000) >> 12)

/* extracts from a virtual address the indices of the corresponding page table */
struct page_indices get_page_indices(UINT64 virtual_address) {
    struct page_indices indices;
    virtual_address >>= 12;
    indices.page = virtual_address & 0x1ff;
    virtual_address >>= 9;
    indices.page_table = virtual_address & 0x1ff;
    virtual_address >>= 9;
    indices.page_directory = virtual_address & 0x1ff;
    virtual_address >>= 9;
    indices.page_directory_pointer = virtual_address & 0x1ff;
    return indices;
}

void MapMemory(struct page_table *PML4, void *virtual_memory, void *physical_memory) {
    struct page_indices indices = get_page_indices((UINT64) virtual_memory);
    UINT64 *PDE; // page directory entry

    PDE = &PML4->entries[indices.page_directory_pointer];
    struct page_table *PDP;
    if (!GetFlag(PDE, PRESENT)) {
        PDP = RequestPage();
        *PDP = (struct page_table) {};
        SetAddress(PDE, (UINT64) PDP >> 12);
        SetFlag(PDE, PRESENT, 1);
        SetFlag(PDE, READ_WRITE, 1);
    } else {
        PDP = (struct page_table *) (get_address(*PDE) << 12);
    }

    PDE = &PDP->entries[indices.page_directory];
    struct page_table *PD;
    if (!GetFlag(PDE, PRESENT)) {
        PD = RequestPage();
        *PD = (struct page_table) {};
        SetAddress(PDE, (UINT64) PD >> 12);
        SetFlag(PDE, PRESENT, 1);
        SetFlag(PDE, READ_WRITE, 1);
    } else {
        PD = (struct page_table *) (get_address(*PDE) << 12);
    }

    PDE = &PD->entries[indices.page_table];
    struct page_table *PT;
    if (!GetFlag(PDE, PRESENT)) {
		PT = RequestPage();
        *PT = (struct page_table) {};
        SetAddress(PDE, (UINT64) PT >> 12);
        SetFlag(PDE, PRESENT, 1);
        SetFlag(PDE, READ_WRITE, 1);
    } else {
        PT = (struct page_table *) (get_address(*PDE) << 12);
    }

    PDE = &PT->entries[indices.page];
    SetAddress(PDE, (UINT64) physical_memory >> 12);
    SetFlag(PDE, PRESENT, 1);
    SetFlag(PDE, READ_WRITE, 1);
}

BOOLEAN CompareStrings(char* a, char* b, UINTN length) {
	for (UINTN i = 0; i < length; i++){
		if (*a++ != *b++)
			return 0;
	}
	return 1;
}

EFI_STATUS Read(EFI_FILE_PROTOCOL *file,
				UINT64             offset,
				UINT64             size,
				void              *dst)
{
	EFI_STATUS status = file->SetPosition(file, offset);
	if (status)
		return status;
	UINT8 *buf  = dst;
	UINTN  read = 0;
	while (read < size) {
		UINTN remains = size - read;
		status = file->Read(file, &remains, buf + read);
		if (status)
			return status;
		read += remains;
	}

	return EFI_SUCCESS;
}

void VerifyELF(Elf64_Ehdr *header) {
	if (header->e_ident[EI_MAG0] != ELFMAG0
	||  header->e_ident[EI_MAG1] != ELFMAG1
	||  header->e_ident[EI_MAG2] != ELFMAG2
	||  header->e_ident[EI_MAG3] != ELFMAG3)
	{
		Error("No ELF magic sequence\r\n");
	}

	if (header->e_ident[EI_CLASS] != ELFCLASS64) {
		Error("Unsupported ELF class (%u)\r\n", header->e_ident);
	}

	if (header->e_type != ET_EXEC && header->e_type != ET_DYN) {
		Error("Unsupported ELF type (%u)\r\n", header->e_type);
	}

	if (header->e_phnum == 0) {
		Error("ELF header does not contain any program headers\r\n");
	}

	if (header->e_phentsize != sizeof(Elf64_Phdr)) {
		Error("Unsupported program header size (%u != %u)\r\n", header->e_phentsize, sizeof(Elf64_Phdr));
	}
}

void ExploreHandle(EFI_HANDLE handle) {
    EFI_GUID **guids;
	UINTN      count;
	EFI_STATUS status = _SystemTable->BootServices->ProtocolsPerHandle(handle, &guids, &count);
	if (status) {
		Error("Return after ProtocolsPerHandle in ExploreHandle");
	}

	for (int i = 0; i < count; i++) {
        EFI_GUID *guid = guids[i];
        Print("{0x%x, 0x%x, 0x%x, {0x%x", guid->Data1, guid->Data2, guid->Data3, guid->Data4[0]);
		for (int j = 1; j < ArrayCount(guid->Data4); j++) {
			Print(", 0x%x", guid->Data4[j]);
		}
		Print("}\r\n");
	}
	status = _SystemTable->BootServices->FreePool(guids);
	if (status) {
		Error("Return after FreePool in ExploreHandle");
	}
}

struct memory_map GetMemoryMap(void) {
	struct memory_map memory_map = {.size = PAGE_SIZE};
	while (1) {
		EFI_STATUS status = _SystemTable->BootServices->AllocatePool(EfiLoaderData, memory_map.size, (void **) &memory_map.address);
		if (status != EFI_SUCCESS) {
			Error("Return after AllocatePool(MemoryMap)\r\n");
		}

		status = _SystemTable->BootServices->GetMemoryMap(&memory_map.size, memory_map.address, &memory_map.key,
														 &memory_map.descriptor_size, &memory_map.descriptor_version);
		if (status == EFI_SUCCESS) {
			break;
		}
		_SystemTable->BootServices->FreePool(memory_map.address);
		if (status == EFI_BUFFER_TOO_SMALL) {
			/*
			 * the pool size must be greater than the known memory map size,
			 * because when we re-allocate space for the memory map in the next iteration,
			 * the memory map itself might change, thus becoming bigger
			 */
			memory_map.size *= 2;
		} else {
			Error("Return after GetMemoryMap\r\n");
		}
	}
	return memory_map;
}

EFI_STATUS EFIAPI main(IN EFI_HANDLE Handle, IN EFI_SYSTEM_TABLE *SystemTable) {
    /* used by other functions, at the moment I found no good reason to pass it to them anyway */
	_SystemTable = SystemTable;

	EFI_STATUS status;
    status = SystemTable->ConOut->ClearScreen(SystemTable->ConOut);
    if (status)
        return status;

    Print("Hello, UEFI!\r\n");

    ExploreHandle(Handle);

	EFI_LOADED_IMAGE_PROTOCOL *image;
    EFI_GUID imguid = EFI_LOADED_IMAGE_PROTOCOL_GUID;
	status = SystemTable->BootServices->OpenProtocol(Handle, &imguid, (void **) &image, Handle, NULL, EFI_OPEN_PROTOCOL_BY_HANDLE_PROTOCOL);
	if (status) {
		Error("Return after opening loaded image protocol\r\n");
	}

#ifdef DEBUG
	Print("Image base address: %p\r\n", image->ImageBase);
	int wait = 1;
    while (wait) {
        __asm__ __volatile__("hlt");
    }
#endif

	/* access EFI system partition (ESP) */

	EFI_SIMPLE_FILE_SYSTEM_PROTOCOL *rootfs;
	EFI_HANDLE root_device = image->DeviceHandle;
    EFI_GUID fsguid = EFI_SIMPLE_FILE_SYSTEM_PROTOCOL_GUID;
    status = SystemTable->BootServices->OpenProtocol(root_device, &fsguid, (void **) &rootfs, Handle, NULL, EFI_OPEN_PROTOCOL_BY_HANDLE_PROTOCOL);
	if (status) {
		Error("Return after opening simple file system protocol\r\n");
	}

	EFI_FILE_PROTOCOL *rootdir;
	status = rootfs->OpenVolume(rootfs, &rootdir);
	if (status) {
		Error("Return after OpenVolume\r\n");
	}

	/* setup ELF program */

	Print("Setting up ELF kernel...\r\n");

	EFI_FILE_PROTOCOL *kernel;
	status = rootdir->Open(rootdir, &kernel, u"EFI\\Boot\\kernel", EFI_FILE_MODE_READ, EFI_FILE_READ_ONLY);
	if (status) {
		Error("Return after Open\r\n");
	}

	// EFI_FILE_INFO info;
	// UINT64 sizeof_info = sizeof(info);
	// EFI_GUID infoid = EFI_FILE_INFO_ID;
	// status = kernel->GetInfo(kernel, &infoid, &sizeof_info, &info);
	// if (status) {
	// 	Error("Return after GetInfo\r\n");
	// }

	Elf64_Ehdr elf_header;
	status = Read(kernel, 0, sizeof(elf_header), &elf_header);
	if (status) {
		Error("Return after Read(elf_header)\r\n");
	}

	VerifyELF(&elf_header);

	Elf64_Phdr *program_headers;
	UINTN ph_size = elf_header.e_phnum * elf_header.e_phentsize;
	status = SystemTable->BootServices->AllocatePool(EfiLoaderData, ph_size, (void **) &program_headers);
	if (status) {
		Error("Return after AllocatePool\r\n");
	}

	status = Read(kernel, elf_header.e_phoff, ph_size, (void *) program_headers);
	if (status) {
		Error("Return after Read(program_headers)\r\n");
	}

	/* load ELF image */

	/* calculate program memory size */
	UINT64 image_begin = UINT64_MAX;
	UINT64 image_end   = 0;
	for (int i = 0; i < elf_header.e_phnum; i++) {
		Elf64_Phdr phdr = program_headers[i];
		if (phdr.p_type != PT_LOAD) {
			continue;
		}
		UINT64 phdr_begin = phdr.p_vaddr;
		phdr_begin &= ~(phdr.p_align - 1);
		if (phdr_begin < image_begin) {
			image_begin = phdr_begin;
		}
		UINT64 phdr_end = phdr.p_vaddr + phdr.p_memsz;
		phdr_end = (phdr_end + phdr.p_align - 1) & ~(phdr.p_align - 1);
		if (phdr_end > image_end) {
			image_end = phdr_end;
		}
	}

	UINT64 image_size  = image_end - image_begin;
	UINTN  image_pages = image_size / PAGE_SIZE + 1;
	EFI_PHYSICAL_ADDRESS image_addr;
	status = SystemTable->BootServices->AllocatePages(AllocateAnyPages, EfiLoaderData, image_pages, &image_addr);
	if (status) {
		Error("Return after AllocatePages\r\n");
	}

	memset((void *) image_addr, 0, image_size);

	for (int i = 0; i < elf_header.e_phnum; i++) {
		Elf64_Phdr phdr = program_headers[i];
		if (phdr.p_type != PT_LOAD) {
			continue;
		}
		UINT64 phdr_addr = image_addr + (phdr.p_vaddr - image_begin);
		status = Read(kernel, phdr.p_offset, phdr.p_filesz, (void *) phdr_addr);
		if (status) {
			Error("Return after Read(program header)\r\n");
		}
	}

	/* setup graphics output */

	EFI_GRAPHICS_OUTPUT_PROTOCOL *gop;
    EFI_GUID gopguid = EFI_GRAPHICS_OUTPUT_PROTOCOL_GUID;
	status = SystemTable->BootServices->LocateProtocol(&gopguid, NULL, (void **) &gop);
	if (status) {
		Error("Return after locating graphics output protocol\r\n");
	}
	struct framebuffer framebuffer = {
		.address = (void*) gop->Mode->FrameBufferBase,
		.size    = gop->Mode->FrameBufferSize,
		.width   = gop->Mode->Info->HorizontalResolution,
		.height  = gop->Mode->Info->VerticalResolution,
		.stride  = gop->Mode->Info->PixelsPerScanLine,
	};
	Print("Framebuffer address 0x%x, size %u, width %u, height %u, stride %u, pixel format %u\r\n",
		  framebuffer.address, framebuffer.size, framebuffer.width, framebuffer.height, framebuffer.stride, gop->Mode->Info->PixelFormat);

	/* read font file */

	EFI_FILE_PROTOCOL *font;
	status = rootdir->Open(rootdir, &font, u"EFI\\Boot\\zap-light16.psf", EFI_FILE_MODE_READ, EFI_FILE_READ_ONLY);
	if (status) {
		Error("Return after Open(font file)\r\n");
	}

	struct PSF1_font psf1;
	status = Read(font, 0, sizeof(struct PSF1_header), &psf1.header);
	if (status) {
		Error("Return after Read(PSF1 header)\r\n");
	}

	if (psf1.header.magic[0] != PSF1_MAGIC0 || psf1.header.magic[1] != PSF1_MAGIC1) {
		Error("The font file is not a valid PSF1 font\r\n");
	}

	UINTN glyph_buffer_size;
	if (psf1.header.mode == 1) { // 512 glyphs
		glyph_buffer_size = psf1.header.char_size * 512;
	} else {					 // 256 glyphs
		glyph_buffer_size = psf1.header.char_size * 256;
	}

	status = SystemTable->BootServices->AllocatePool(EfiLoaderData, glyph_buffer_size, (void **) &psf1.glyph_buffer);
	if (status) {
		Error("Return after AllocatePool(glyph_buffer)\r\n");
	}

	status = Read(font, sizeof(struct PSF1_header), glyph_buffer_size, psf1.glyph_buffer);
	if (status) {
		Error("Return after Read(glyph_buffer)\r\n");
	}

	Print("Font found, char size: %u\r\n", psf1.header.char_size);

	/*
	 * look for the first RSDP revision 2 (ACPI revision >= 2.0)
	 *
	 * something strange can happen, both on real hardware and QEMU:
	 * the VendorGUID associated to the RSDP may not actually be the correct one, as I have found:
	 * - an RSDP revision 0 (ACPI 1.0) associated to an EFI_ACPI_20_TABLE_GUID
	 * - an RSDP revision 2 associated to an ACPI_10_TABLE_GUID
	 * - an RSDP revision 2 associated to both GUIDs
	 */
	EFI_GUID ACPI1 = ACPI_10_TABLE_GUID;
	EFI_GUID ACPI2 = EFI_ACPI_20_TABLE_GUID;
	struct RSDP *RSDP = NULL;
	for (UINTN i = 0; i < SystemTable->NumberOfTableEntries; i++) {
		EFI_CONFIGURATION_TABLE Entry = SystemTable->ConfigurationTable[i];
		if (!CompareGuids(&Entry.VendorGuid, &ACPI1) && !CompareGuids(&Entry.VendorGuid, &ACPI2)) {
			continue;
		}
		struct RSDP *Table = (struct RSDP *) Entry.VendorTable;
		if (CompareStrings(Entry.VendorTable, "RSD PTR ", 8) && Table->Revision == 2) {
			RSDP = Table;
			break;
		}
	}
	if (!RSDP) {
		Error("Could not find an ACPI 2.0 table, aborting\r\n");
	}

	/* build new page map to map the kernel to the higher half of the memory */

	struct page_table _PML4 = {};
	struct page_table *PML4 = &_PML4;

	struct memory_map memory_map = GetMemoryMap();
	UINT64 memory_size = 0;
    UINTN  map_entries = memory_map.size / memory_map.descriptor_size;
	for (UINTN i = 0; i < map_entries; i++) {
        /*
         * the UEFI Specification Version 2.10, section 7.2.3, requires to use the descriptor size
         * to find the base address of each entry
         */
        EFI_MEMORY_DESCRIPTOR *md = (EFI_MEMORY_DESCRIPTOR *) ((UINTN) memory_map.address + i * memory_map.descriptor_size);
        memory_size += md->NumberOfPages * PAGE_SIZE;
    }

	for (UINT64 i = 0; i < memory_size; i += PAGE_SIZE) {
        MapMemory(PML4, (void *) i, (void *) i);
    }

    for (UINT64 i = 0; i < framebuffer.size; i += PAGE_SIZE) {
        MapMemory(PML4, framebuffer.address + i, framebuffer.address + i);
    }

	for (EFI_VIRTUAL_ADDRESS i = 0; i < image_size; i += PAGE_SIZE) {
		MapMemory(PML4, (void *) (KERNEL_BASE + i), (void *) (image_addr + i + PAGE_SIZE));
	}

	/* obtain memory map, shut down boot services and start the kernel */

	Print("Now it's time to leave the capsule, if you dare\r\n");

	memory_map = GetMemoryMap();

	status = SystemTable->BootServices->ExitBootServices(Handle, memory_map.key);
	if (status) {
		Error("Return after ExitBootServices\r\n");
	}
	/* required by the UEFI Specification Version 2.10, section 7.4.6 */
	SystemTable->ConsoleInHandle     = NULL;
	SystemTable->ConIn               = NULL;
	SystemTable->ConsoleOutHandle    = NULL;
	SystemTable->ConOut              = NULL;
	SystemTable->StandardErrorHandle = NULL;
	SystemTable->StdErr              = NULL;
	SystemTable->BootServices        = NULL; // same paragraph, spec. calls it "BootServicesTable"
	// TODO: the CRC32 of the System Table must be recomputed (same paragraph)

	/* use the new page map with the kernel mapped to the higher half of the memory */
	__asm__("mov %0, %%cr3" : : "r" (PML4));

	ENTRY StartKernel = (ENTRY) elf_header.e_entry;
	struct boot_info boot_info = {
		.framebuffer = framebuffer,
		.font = psf1,
		.memory_map = memory_map,
		.RSDP = RSDP,
		.PML4 = PML4,
	};
	StartKernel(&boot_info);

	while (1) {};

    return EFI_LOAD_ERROR;
}
