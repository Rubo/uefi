#ifndef UEFI_H
#define UEFI_H

#include <stdbool.h>
#include <stdint.h>

typedef bool        BOOLEAN;
typedef uint64_t    UINTN; // for 64-bit processors only
typedef uint8_t     UINT8;
typedef int16_t     INT16;
typedef uint16_t    UINT16;
typedef int32_t     INT32;
typedef uint32_t    UINT32;
typedef uint64_t    UINT64;
typedef __uint128_t UINT128;
typedef uint8_t     CHAR8;
typedef uint16_t    CHAR16;
typedef UINTN       EFI_STATUS;
typedef void *      EFI_HANDLE;

#define IN
#define OUT
#define OPTIONAL
#define CONST    const
#define EFIAPI   __attribute__((ms_abi))

#define EFI_SUCCESS           0x0
#define EFI_LOAD_ERROR        0x8000000000000001
#define EFI_INVALID_PARAMETER 0x8000000000000002
#define EFI_UNSUPPORTED       0x8000000000000003
#define EFI_BUFFER_TOO_SMALL  0x8000000000000005
#define EFI_OUT_OF_RESOURCES  0x8000000000000009

typedef struct EFI_TABLE_HEADER {
    UINT64 Signature;
    UINT32 Revision;
    UINT32 HeaderSize;
    UINT32 CRC32;
    UINT32 Reserved;
} EFI_TABLE_HEADER;

typedef struct EFI_GUID EFI_GUID;
struct EFI_GUID {
    UINT32 Data1;
	UINT16 Data2;
	UINT16 Data3;
	UINT8  Data4[8];
};

typedef struct {
    UINT16 Year;       // 1900 - 9999
    UINT8  Month;      // 1 - 12
    UINT8  Day;        // 1 - 31
    UINT8  Hour;       // 0 - 23
    UINT8  Minute;     // 0 - 59
    UINT8  Second;     // 0 - 59
    UINT8  Pad1;
    UINT32 Nanosecond; // 0 - 999,999,999
    INT16  TimeZone;
    UINT8  Daylight;
    UINT8  Pad2;
} EFI_TIME;

typedef struct EFI_SIMPLE_TEXT_OUTPUT_PROTOCOL EFI_SIMPLE_TEXT_OUTPUT_PROTOCOL;
typedef EFI_STATUS (EFIAPI *EFI_TEXT_STRING)(IN EFI_SIMPLE_TEXT_OUTPUT_PROTOCOL *This,
                                             IN CHAR16                          *String);
typedef EFI_STATUS (EFIAPI *EFI_TEXT_CLEAR_SCREEN)(IN EFI_SIMPLE_TEXT_OUTPUT_PROTOCOL *This);
struct EFI_SIMPLE_TEXT_OUTPUT_PROTOCOL {
    void (*unused1)();
    EFI_TEXT_STRING OutputString;
    void (*unused2)();
    void (*unused3)();
    void (*unused4)();
    void (*unused5)();
    EFI_TEXT_CLEAR_SCREEN ClearScreen;
    void (*unused6)();
    void (*unused7)();
    void  *unused8;
};

#define EFI_OPEN_PROTOCOL_BY_HANDLE_PROTOCOL  0x00000001
#define EFI_OPEN_PROTOCOL_GET_PROTOCOL        0x00000002
#define EFI_OPEN_PROTOCOL_TEST_PROTOCOL       0x00000004
#define EFI_OPEN_PROTOCOL_BY_CHILD_CONTROLLER 0x00000008
#define EFI_OPEN_PROTOCOL_BY_DRIVER           0x00000010
#define EFI_OPEN_PROTOCOL_EXCLUSIVE           0x00000020
typedef enum {
    AllocateAnyPages,
    AllocateMaxAddress,
    AllocateAddress,
    MaxAllocateType
} EFI_ALLOCATE_TYPE;
/*
 * EFI_MEMORY_TYPE
 * These type values are discussed in Memory Type Usage before ExitBootServices()
 * and Memory Type Usage after ExitBootServices().
 */
typedef enum {
    EfiReservedMemoryType,
    EfiLoaderCode,
    EfiLoaderData,
    EfiBootServicesCode,
    EfiBootServicesData,
    EfiRuntimeServicesCode,
    EfiRuntimeServicesData,
    EfiConventionalMemory,
    EfiUnusableMemory,
    EfiACPIReclaimMemory,
    EfiACPIMemoryNVS,
    EfiMemoryMappedIO,
    EfiMemoryMappedIOPortSpace,
    EfiPalCode,
    EfiPersistentMemory,
    EfiUnacceptedMemoryType,
    EfiMaxMemoryType
} EFI_MEMORY_TYPE;
typedef UINT64 EFI_PHYSICAL_ADDRESS;
typedef UINT64 EFI_VIRTUAL_ADDRESS;
typedef struct {
    UINT32               Type;
    EFI_PHYSICAL_ADDRESS PhysicalStart;
    EFI_VIRTUAL_ADDRESS  VirtualStart;
    UINT64               NumberOfPages;
    UINT64               Attribute;
} EFI_MEMORY_DESCRIPTOR;
typedef enum {
    AllHandles,
    ByRegisterNotify,
    ByProtocol
} EFI_LOCATE_SEARCH_TYPE;
typedef EFI_STATUS (EFIAPI *EFI_ALLOCATE_PAGES)(IN     EFI_ALLOCATE_TYPE     Type,
                                                IN     EFI_MEMORY_TYPE       MemoryType,
                                                IN     UINTN                 Pages,
                                                IN OUT EFI_PHYSICAL_ADDRESS *Memory);
typedef EFI_STATUS (EFIAPI *EFI_FREE_PAGES)(IN EFI_PHYSICAL_ADDRESS Memory,
                                            IN UINTN                Pages);
typedef EFI_STATUS (EFIAPI *EFI_GET_MEMORY_MAP)(IN OUT UINTN                 *MemoryMapSize,
                                                OUT    EFI_MEMORY_DESCRIPTOR *MemoryMap,
                                                OUT    UINTN                 *MapKey,
                                                OUT    UINTN                 *DescriptorSize,
                                                OUT    UINT32                *DescriptorVersion);
typedef EFI_STATUS (EFIAPI *EFI_ALLOCATE_POOL)(IN  EFI_MEMORY_TYPE PoolType,
                                               IN  UINTN           Size,
                                               OUT void          **Buffer);
typedef EFI_STATUS (EFIAPI *EFI_FREE_POOL)(IN void *Buffer);
typedef EFI_STATUS (EFIAPI *EFI_HANDLE_PROTOCOL)(IN  EFI_HANDLE Handle,
                                                 IN  EFI_GUID  *Protocol,
                                                 OUT void     **Interface);
typedef EFI_STATUS (EFIAPI *EFI_PROTOCOLS_PER_HANDLE)(IN  EFI_HANDLE  Handle,
                                                      OUT EFI_GUID ***ProtocolBuffer,

                                                      OUT UINTN      *ProtocolBufferCount);
typedef EFI_STATUS (EFIAPI *EFI_LOCATE_HANDLE_BUFFER)(IN  EFI_LOCATE_SEARCH_TYPE SearchType,
                                                      IN  EFI_GUID              *Protocol OPTIONAL,
                                                      IN  void                  *SearchKey OPTIONAL,
                                                      OUT UINTN                 *NoHandles,
                                                      OUT EFI_HANDLE           **Buffer);
typedef EFI_STATUS (EFIAPI *EFI_LOCATE_PROTOCOL)(IN  EFI_GUID *Protocol,
                                                 IN  void     *Registration OPTIONAL,
                                                 OUT void    **Interface);
typedef EFI_STATUS (EFIAPI *EFI_EXIT)(IN EFI_HANDLE ImageHandle,
                                      IN EFI_STATUS ExitStatus,
                                      IN UINTN      ExitDataSize,
                                      IN CHAR16    *ExitData OPTIONAL);
typedef EFI_STATUS (EFIAPI *EFI_EXIT_BOOT_SERVICES)(IN EFI_HANDLE ImageHandle,
                                                    IN UINTN      MapKey);
typedef EFI_STATUS (EFIAPI *EFI_OPEN_PROTOCOL)(IN  EFI_HANDLE Handle,
                                               IN  EFI_GUID  *Protocol,
                                               OUT void     **Interface OPTIONAL,
                                               IN  EFI_HANDLE AgentHandle,
                                               IN  EFI_HANDLE ControllerHandle,
                                               IN  UINT32     Attributes);
typedef struct EFI_BOOT_SERVICES {
    EFI_TABLE_HEADER Hdr;

    // Task Priority Services
    void (*unused1)();
    void (*unused2)();

    // Memory Services
    EFI_ALLOCATE_PAGES AllocatePages;
    EFI_FREE_PAGES     FreePages;
    EFI_GET_MEMORY_MAP GetMemoryMap;
    EFI_ALLOCATE_POOL  AllocatePool;
    EFI_FREE_POOL      FreePool;

    // Event & Timer Services
    void (*unused7)();
    void (*unused8)();
    void (*unused9)();
    void (*unused10)();
    void (*unused11)();
    void (*unused12)();

    // Protocol Handler Services
    void (*unused13)();
    void (*unused14)();
    void (*unused15)();
    EFI_HANDLE_PROTOCOL HandleProtocol;
    void *reserved;
    void (*unused17)();
    void (*unused18)();
    void (*unused19)();
    void (*unused20)();

    // Image Services
    void (*unused21)();
    void (*unused22)();
    EFI_EXIT               Exit;
    void (*unused24)();
    EFI_EXIT_BOOT_SERVICES ExitBootServices;

    // Miscellaneous Services #1
    void (*unused26)();
    void (*unused27)();
    void (*unused28)();

    // DriverSupport Services
    void (*unused29)();
    void (*unused30)();

    // Open and Close Protocol Services
    EFI_OPEN_PROTOCOL OpenProtocol;
    void (*unused32)();
    void (*unused33)();

    // Library Services
    EFI_PROTOCOLS_PER_HANDLE ProtocolsPerHandle;
    EFI_LOCATE_HANDLE_BUFFER LocateHandleBuffer;
    EFI_LOCATE_PROTOCOL      LocateProtocol;
    void (*unused36)();
    void (*unused37)();

    // 32-bit CRC Services
    void (*unused38)();

    // Miscellaneous Services #2
    void (*unused39)();
    void (*unused40)();
    void (*unused41)();
} EFI_BOOT_SERVICES;

#define EFI_FILE_PROTOCOL_REVISION        0x00010000
#define EFI_FILE_PROTOCOL_REVISION2       0x00020000
#define EFI_FILE_PROTOCOL_LATEST_REVISION EFI_FILE_PROTOCOL_REVISION2
// Open Modes
#define EFI_FILE_MODE_READ   0x0000000000000001
#define EFI_FILE_MODE_WRITE  0x0000000000000002
#define EFI_FILE_MODE_CREATE 0x8000000000000000
// File Attributes
#define EFI_FILE_READ_ONLY  0x0000000000000001
#define EFI_FILE_HIDDEN     0x0000000000000002
#define EFI_FILE_SYSTEM     0x0000000000000004
#define EFI_FILE_RESERVED   0x0000000000000008
#define EFI_FILE_DIRECTORY  0x0000000000000010
#define EFI_FILE_ARCHIVE    0x0000000000000020
#define EFI_FILE_VALID_ATTR 0x0000000000000037
typedef struct EFI_FILE_PROTOCOL EFI_FILE_PROTOCOL;
typedef EFI_STATUS (EFIAPI *EFI_FILE_OPEN)(IN  EFI_FILE_PROTOCOL  *This,
                                           OUT EFI_FILE_PROTOCOL **NewHandle,
                                           IN  CHAR16             *FileName,
                                           IN  UINT64              OpenMode,
                                           IN  UINT64              Attributes);
typedef EFI_STATUS (EFIAPI *EFI_FILE_CLOSE)(IN EFI_FILE_PROTOCOL *This);
typedef EFI_STATUS (EFIAPI *EFI_FILE_READ)(IN     EFI_FILE_PROTOCOL *This,
                                           IN OUT UINTN             *BufferSize,
                                           OUT    void              *Buffer);
typedef EFI_STATUS (EFIAPI *EFI_FILE_GET_POSITION)(IN  EFI_FILE_PROTOCOL *This,
                                                   OUT UINT64            *Position);
typedef EFI_STATUS (EFIAPI *EFI_FILE_SET_POSITION)(IN EFI_FILE_PROTOCOL *This,
                                                   IN UINT64             Position);
typedef EFI_STATUS (EFIAPI *EFI_FILE_GET_INFO)(IN     EFI_FILE_PROTOCOL *This,
                                               IN     EFI_GUID          *InformationType,
                                               IN OUT UINTN             *BufferSize,
                                               OUT    void              *Buffer);
struct EFI_FILE_PROTOCOL {
    UINT64                Revision;
    EFI_FILE_OPEN         Open;
    EFI_FILE_CLOSE        Close;
    void (*unused1)();
    EFI_FILE_READ         Read;
    void (*unused2)();
    EFI_FILE_GET_POSITION GetPosition;
    EFI_FILE_SET_POSITION SetPosition;
    EFI_FILE_GET_INFO     GetInfo;
    void (*unused5)();
    void (*unused6)();
    void (*unused7)();
    void (*unused8)();
    void (*unused9)();
    void (*unused10)();
};

#define EFI_FILE_INFO_ID   {0x09576e92, 0x6d3f, 0x11d2, {0x8e, 0x39, 0x00, 0xa0, 0xc9, 0x69, 0x72, 0x3b}}
#define EFI_FILE_READ_ONLY  0x0000000000000001
#define EFI_FILE_HIDDEN     0x0000000000000002
#define EFI_FILE_SYSTEM     0x0000000000000004
#define EFI_FILE_RESERVED   0x0000000000000008
#define EFI_FILE_DIRECTORY  0x0000000000000010
#define EFI_FILE_ARCHIVE    0x0000000000000020
#define EFI_FILE_VALID_ATTR 0x0000000000000037
typedef struct EFI_FILE_INFO {
    UINT64   Size; // size of the struct, including the NULL-terminated FileName
    UINT64   FileSize;
    UINT64   PhysicalSize;
    EFI_TIME CreateTime;
    EFI_TIME LastAccessTime;
    EFI_TIME ModificationTime;
    UINT64   Attribute;
    CHAR16   FileName[256];
} EFI_FILE_INFO;

#define EFI_SIMPLE_FILE_SYSTEM_PROTOCOL_GUID     {0x0964e5b22, 0x6459, 0x11d2, {0x8e, 0x39, 0x00, 0xa0, 0xc9, 0x69, 0x72, 0x3b}}
#define EFI_SIMPLE_FILE_SYSTEM_PROTOCOL_REVISION  0x00010000
typedef struct EFI_SIMPLE_FILE_SYSTEM_PROTOCOL EFI_SIMPLE_FILE_SYSTEM_PROTOCOL;
typedef EFI_STATUS(EFIAPI *EFI_SIMPLE_FILE_SYSTEM_PROTOCOL_OPEN_VOLUME)(IN  EFI_SIMPLE_FILE_SYSTEM_PROTOCOL *This,
                                                                        OUT EFI_FILE_PROTOCOL **Root);
struct EFI_SIMPLE_FILE_SYSTEM_PROTOCOL {
    UINT64                                      Revision;
    EFI_SIMPLE_FILE_SYSTEM_PROTOCOL_OPEN_VOLUME OpenVolume;
};

//
// ACPI 2.0 or newer tables should use EFI_ACPI_TABLE_GUID
//
#define EFI_ACPI_TABLE_GUID {0x8868e871, 0xe4f1, 0x11d3, {0xbc, 0x22, 0x00, 0x80, 0xc7, 0x3c, 0x88, 0x81}}
#define EFI_ACPI_20_TABLE_GUID EFI_ACPI_TABLE_GUID
#define ACPI_TABLE_GUID {0xeb9d2d30, 0x2d88, 0x11d3, {0x9a, 0x16, 0x00, 0x90, 0x27, 0x3f, 0xc1, 0x4d}}
#define ACPI_10_TABLE_GUID ACPI_TABLE_GUID
typedef struct {
    EFI_GUID VendorGuid;
    void    *VendorTable;
} EFI_CONFIGURATION_TABLE;

typedef struct EFI_SYSTEM_TABLE {
    EFI_TABLE_HEADER                 Hdr;
    CHAR16                          *unused1;
    UINT32                           unused2;
    EFI_HANDLE                       ConsoleInHandle;
    void                            *ConIn;
    EFI_HANDLE                       ConsoleOutHandle;
    EFI_SIMPLE_TEXT_OUTPUT_PROTOCOL *ConOut;
    EFI_HANDLE                       StandardErrorHandle;
    EFI_SIMPLE_TEXT_OUTPUT_PROTOCOL *StdErr;
    void                            *unused8;
    EFI_BOOT_SERVICES               *BootServices;
    UINTN                            NumberOfTableEntries;
    EFI_CONFIGURATION_TABLE         *ConfigurationTable;
} EFI_SYSTEM_TABLE;

#define EFI_DEVICE_PATH_PROTOCOL_GUID {0x09576e91, 0x6d3f, 0x11d2, {0x8e, 0x39, 0x00, 0xa0, 0xc9, 0x69, 0x72, 0x3b}}
typedef struct EFI_DEVICE_PATH_PROTOCOL {
    UINT8 Type;
    UINT8 SubType;
    UINT8 Length[2];
} EFI_DEVICE_PATH_PROTOCOL;

#define EFI_LOADED_IMAGE_PROTOCOL_GUID    {0x5B1B31A1, 0x9562, 0x11d2, {0x8E, 0x3F, 0x00, 0xA0, 0xC9, 0x69, 0x72, 0x3B}}
#define EFI_LOADED_IMAGE_PROTOCOL_REVISION 0x1000
typedef struct EFI_LOADED_IMAGE_PROTOCOL {
    UINT32            Revision;
    EFI_HANDLE        ParentHandle;
    EFI_SYSTEM_TABLE *SystemTable;

    // Source location of the image
    EFI_HANDLE                DeviceHandle;
    EFI_DEVICE_PATH_PROTOCOL *FilePath;
    void                     *Reserved;

    // Image’s load options
    UINT32 LoadOptionsSize;
    void  *LoadOptions;

    // Location where image was loaded
    void           *ImageBase;
    UINT64          ImageSize;
    EFI_MEMORY_TYPE ImageCodeType;
    EFI_MEMORY_TYPE ImageDataType;
    void (*unused)();
} EFI_LOADED_IMAGE_PROTOCOL;

#define EFI_GRAPHICS_OUTPUT_PROTOCOL_GUID {0x9042a9de, 0x23dc, 0x4a38, {0x96, 0xfb, 0x7a, 0xde, 0xd0, 0x80, 0x51, 0x6a}}
typedef struct EFI_GRAPHICS_OUTPUT_PROTOCOL         EFI_GRAPHICS_OUTPUT_PROTOCOL;
typedef struct EFI_GRAPHICS_OUTPUT_MODE_INFORMATION EFI_GRAPHICS_OUTPUT_MODE_INFORMATION;
typedef EFI_STATUS (EFIAPI *EFI_GRAPHICS_OUTPUT_PROTOCOL_QUERY_MODE)(IN  EFI_GRAPHICS_OUTPUT_PROTOCOL          *This,
                                                                     IN  UINT32                                 ModeNumber,
                                                                     OUT UINTN                                 *SizeOfInfo,
                                                                     OUT EFI_GRAPHICS_OUTPUT_MODE_INFORMATION **Info);
typedef struct {
    UINT32 RedMask;
    UINT32 GreenMask;
    UINT32 BlueMask;
    UINT32 ReservedMask;
} EFI_PIXEL_BITMASK;
typedef enum {
    PixelRedGreenBlueReserved8BitPerColor,
    PixelBlueGreenRedReserved8BitPerColor,
    PixelBitMask,
    PixelBltOnly,
    PixelFormatMax
} EFI_GRAPHICS_PIXEL_FORMAT;
struct EFI_GRAPHICS_OUTPUT_MODE_INFORMATION {
    UINT32                    Version;
    UINT32                    HorizontalResolution;
    UINT32                    VerticalResolution;
    EFI_GRAPHICS_PIXEL_FORMAT PixelFormat;
    EFI_PIXEL_BITMASK         PixelInformation;
    UINT32                    PixelsPerScanLine;
};
typedef struct {
    UINT32                                MaxMode;
    UINT32                                Mode;
    EFI_GRAPHICS_OUTPUT_MODE_INFORMATION *Info;
    UINTN                                 SizeOfInfo;
    EFI_PHYSICAL_ADDRESS                  FrameBufferBase;
    UINTN                                 FrameBufferSize;
} EFI_GRAPHICS_OUTPUT_PROTOCOL_MODE;
struct EFI_GRAPHICS_OUTPUT_PROTOCOL {
    EFI_GRAPHICS_OUTPUT_PROTOCOL_QUERY_MODE QueryMode;
    void (*unused2)();
    void (*unused3)();
    EFI_GRAPHICS_OUTPUT_PROTOCOL_MODE      *Mode;
};

#endif
