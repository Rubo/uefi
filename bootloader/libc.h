#include <stdarg.h>
#include <stddef.h>
#include <stdint.h>

void *memcpy(void *dst, const void *src, size_t size);
void *memset(void *ptr, int value, size_t size);
int u16snprintf(uint16_t *buffer, size_t size, const char *fmt, ...);
