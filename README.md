*Never trust the firmware.*

This repository contains code for experiments with x64 UEFI applications. The target UEFI specification release is [2.10](https://uefi.org/specs/UEFI/2.10).

The UEFI application is a bootloader, which starts an ELF64 kernel that can draw glyphs onto the screen.

The code has been developed and tested on Arch Linux x86-64.

Tools required:

* `clang`
* `gcc`
* `ld`
* `make`
* `nasm`
* `qemu-system-x86_64`

In the root directory of the repository, you are also required to put:

* the UEFI firmware for QEMU: `OVMF.fd`, see [here](https://github.com/tianocore/tianocore.github.io/wiki/OVMF).
* the font file so that the kernel can draw to the screen: `zap-light16.psf`, available [here](https://www.zap.org.au/projects/console-fonts-zap).

To build the application, run:

```sh
sh build.sh
```

The script will create the `build` directory, where the bootloader and the kernel will be built. It will also create the`EFI` directory, which mimics the structure of an EFI system partition (ESP), where the built files will be copied, together with the font file.

To test the application with QEMU, run:

```sh
make run
```

In the Makefile, you can edit the OVMF path by changing the `OVMF` variable.

The application can be executed on real hardware: create an EFI system partition on your preferred data storage device, copy the `EFI` directory in the device's ESP and then boot it.
