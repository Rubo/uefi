#include <stdarg.h>
#include <stddef.h>
#include <stdint.h>

size_t strlen(const char *str);
int strncmp(const char *s1, const char *s2, size_t n);
void *memcpy(void *dst, const void *src, size_t size);
void *memset(void *ptr, int value, size_t size);
int isdigit(int code);
int snprintf(char *buffer, size_t size, const char *fmt, ...);
