[bits 64]
load_GDT:
    lgdt [rdi] ; rdi contains the first parameter of the function as called from C
    mov ax, 0x10
    mov ds, ax
    mov es, ax
    mov fs, ax
    mov gs, ax
    mov ss, ax
    pop rdi ; store return address on rdi
    mov rax, 0x08
    push rax ; used by the far jump to return
    push rdi
    retfq
GLOBAL load_GDT
