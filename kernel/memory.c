#include "common.h"
#include "libc.h"
#include "memory.h"

struct bitmap {
    size_t   size;
    uint8_t *buf;
};

struct page_indices {
    uint64_t page_directory_pointer;
    uint64_t page_directory;
    uint64_t page_table;
    uint64_t page;
};

enum page_table_flag {
    PRESENT = 0,
    READ_WRITE = 1,
    USER_SUPER = 2,
    WRITE_THROUGH = 3,
    CACHE_DISABLED = 4,
    ACCESSED = 5,
    LARGER_PAGES = 7,
    CUSTOM0 = 9,
    CUSTOM1 = 10,
    CUSTOM2 = 11,
    NO_EXECUTE = 63, // supported only in some systems
};

inline uint8_t inb(uint32_t port) {
    uint8_t data;
    __asm__ __volatile__("inb %w1, %b0" : "=a" (data) : "Nd" (port));
    return data;
}

inline uint16_t inw(uint32_t port) {
    uint16_t data;
    __asm__ __volatile__("inw %w1, %w0" : "=a" (data) : "Nd" (port));
    return data;
}

inline uint32_t inl(uint32_t port) {
    uint32_t data;
    __asm__ __volatile__("inl %w1, %0" : "=a" (data) : "Nd" (port));
    return data;
}

inline void outb(uint32_t port, uint8_t data) {
    __asm__ __volatile__("outb %b0, %w1" : : "a" (data), "Nd" (port));
}

inline void outw(uint32_t port, uint16_t data) {
    __asm__ __volatile__("outw %w0, %w1" : : "a" (data), "Nd" (port));
}

inline void outl(uint32_t port, uint32_t data) {
    __asm__ __volatile__("outl %0, %w1" : : "a" (data), "Nd" (port));
}

/*
 * some devices on older machines on the I/O port are slow and we need to wait for them
 * to complete before sending them more commands
 */
void wait_IO(void) {
    /*
     * we send a value to an unused I/O port, wasting one I/O cycle,
     * so that any device that we have communicated with has time to catch up
     */
    __asm__ __volatile__("outb %%al, $0x80" : : "a" (0));
}

inline void mmoutb(uintptr_t p, uint8_t data) {
    *(volatile uint8_t *)(p) = data;
}

inline void mmoutw(uintptr_t p, uint16_t data) {
    *(volatile uint16_t *)(p) = data;
}

inline void mmoutl(uintptr_t p, uint32_t data) {
    *(volatile uint32_t *)(p) = data;
}

inline void mmoutll(uintptr_t p, uint64_t data) {
    *(volatile uint64_t *)(p) = data;
}

inline void mmout(void *dst, const volatile void *src, size_t length) {
    volatile uint8_t *s = (volatile uint8_t *) src;
    uint8_t *d = (uint8_t *) dst;
    while (length > 0) {
        *d =  *s;
        s++;
        d++;
        length--;
    }
}

inline uint8_t mminb(uintptr_t p) {
    return *(volatile uint8_t *)(p);
}

inline uint16_t mminw(uintptr_t p) {
    return *(volatile uint16_t *)(p);
}

inline uint32_t mminl(uintptr_t p) {
    return *(volatile uint32_t *)(p);
}

inline uint64_t mminll(uintptr_t p) {
    return *(volatile uint64_t *)(p);
}

void set_flag(uint64_t *entry, enum page_table_flag flag, bool value) {
    *entry |= (uint64_t) value << flag;
}

bool get_flag(uint64_t *entry, enum page_table_flag flag) {
    bool value = *entry & (uint64_t) 1 << flag;
    return value;
}

void set_address(uint64_t *entry, uint64_t address) {
    address &= 0x000000ffffffffff;
    *entry &= 0xfff0000000000fff;
    *entry |= address << 12;
}

#define get_address(entry) ((entry & 0x000ffffffffff000) >> 12)

bool get_bit(struct bitmap bitmap, size_t index) {
    if (index >= bitmap.size * 8) {
        return false;
    }

    size_t byte_index = index / 8;
    size_t bit_index  = index % 8;
    uint8_t mask = 0b10000000 >> bit_index;
    bool result = bitmap.buf[byte_index] & mask;
    return result;
}

bool set_bit(struct bitmap bitmap, size_t index, bool value) {
    if (index >= bitmap.size * 8) {
        return false;
    }

    size_t byte_index = index / 8;
    size_t bit_index  = index % 8;
    uint8_t mask = 0b10000000 >> bit_index;
    bitmap.buf[byte_index] |= mask;
    return true;
}

uint64_t free_memory = 0;
uint64_t used_memory = 0;
uint64_t reserved_memory = 0;
bool initialised = false;
struct bitmap pages;
size_t last_used_page = 0;

void lock_pages(void *address, size_t count) {
    for (size_t i = 0; i < count; i++) {
        size_t index = (uint64_t) address / PAGE_SIZE + i;
        if (!get_bit(pages, index) && set_bit(pages, index, 1)) {
            used_memory += PAGE_SIZE;
            free_memory -= PAGE_SIZE;
        }
    }
}

void free_pages(void *address, size_t count) {
    for (size_t i = 0; i < count; i++) {
        size_t index = (uint64_t) address / PAGE_SIZE + i;
        if (!get_bit(pages, index)) {
            continue;
        }
        set_bit(pages, index, 0);
        used_memory -= PAGE_SIZE;
        free_memory += PAGE_SIZE;
        if (last_used_page > index) {
            last_used_page = index;
        }
    }
}

void reserve_pages(void *address, size_t count) {
    for (size_t i = 0; i < count; i++) {
        size_t index = (uint64_t) address / PAGE_SIZE + i;
        if (!get_bit(pages, index) && set_bit(pages, index, 1)) {
            reserved_memory += PAGE_SIZE;
            free_memory -= PAGE_SIZE;
        }
    }
}

void unreserve_pages(void *address, size_t count) {
    for (size_t i = 0; i < count; i++) {
        size_t index = (uint64_t) address / PAGE_SIZE + i;
        if (!get_bit(pages, index)) {
            continue;
        }
        set_bit(pages, index, 0);
        reserved_memory -= PAGE_SIZE;
        free_memory += PAGE_SIZE;
        if (last_used_page > index) {
            last_used_page = index;
        }
    }
}

#define lock_page(address) lock_pages(address, 1)
#define free_page(address) free_pages(address, 1)
#define reserve_page(address) reserve_pages(address, 1)
#define unreserve_page(address) unreserve_pages(address, 1)

void * request_page(void) {
    for (; last_used_page < pages.size; last_used_page++) {
        if (!get_bit(pages, last_used_page)) {
            lock_page((void *) (last_used_page * PAGE_SIZE));
            return (void *) (last_used_page * PAGE_SIZE);
        }
    }
    // TODO: we ran out of available pages, swap some to a file in disk
    return NULL;
}

void read_memory_map(struct memory_map memory_map) {
    if (initialised) {
        return;
    }
    initialised = true;

    /*
     * we store the pages bitmap in the largest page of EfiConventionalMemory,
     * and also we compute the total memory size
     */
    void  *largest_page = NULL;
    size_t largest_page_size = 0;
    size_t memory_size = 0;
    UINTN map_entries = memory_map.size / memory_map.descriptor_size;
    for (UINTN i = 0; i < map_entries; i++) {
        /*
         * the UEFI Specification Version 2.10, section 7.2.3, requires to use the descriptor size
         * to find the base address of each entry
         */
        EFI_MEMORY_DESCRIPTOR *md = (EFI_MEMORY_DESCRIPTOR *) ((UINTN) memory_map.address + i * memory_map.descriptor_size);
        if (md->Type == EfiConventionalMemory && largest_page_size < md->NumberOfPages * PAGE_SIZE) {
            largest_page = (void *) md->PhysicalStart;
            largest_page_size = md->NumberOfPages * PAGE_SIZE;
        }
        memory_size += md->NumberOfPages * PAGE_SIZE;
    }
    free_memory = memory_size;
    pages.size = memory_size / PAGE_SIZE / 8 + 1,
    pages.buf  = largest_page,
    memset(pages.buf, 0, pages.size);

    /*
     * first reserve all memory, and then unreserve conventional pages only;
     * it's better this way than the reverse, because sometimes the uefi memory map has gaps,
     * so bits that need be reserved don't become reserved
     */
    // reserve_pages(0, memory_size / PAGE_SIZE + 1);
    // for (UINTN i = 0; i < map_entries; i++) {
    //     EFI_MEMORY_DESCRIPTOR *md = (EFI_MEMORY_DESCRIPTOR *) ((UINTN) memory_map.address + i * memory_map.descriptor_size);
    //     if (md->Type == EfiConventionalMemory) {
    //         unreserve_pages((void *) md->PhysicalStart, md->NumberOfPages);
    //     }
    // }

    lock_pages(pages.buf, pages.size / PAGE_SIZE + 1);

    /* reserve non conventional pages */
    for (UINTN i = 0; i < map_entries; i++) {
        EFI_MEMORY_DESCRIPTOR *md = (EFI_MEMORY_DESCRIPTOR *) ((UINTN) memory_map.address + i * memory_map.descriptor_size);
        if (md->Type != EfiConventionalMemory) {
            reserve_pages((void *) md->PhysicalStart, md->NumberOfPages);
        }
    }

    /*
     * reserve between 0 and 0x1000000, needed on some systems
     * because it is were are stored some firmware data
     */
    reserve_pages(0, 0x100);
}

/* extracts from a virtual address the indices of the corresponding page table */
struct page_indices get_page_indices(uint64_t virtual_address) {
    struct page_indices indices;
    virtual_address >>= 12;
    indices.page = virtual_address & 0x1ff;
    virtual_address >>= 9;
    indices.page_table = virtual_address & 0x1ff;
    virtual_address >>= 9;
    indices.page_directory = virtual_address & 0x1ff;
    virtual_address >>= 9;
    indices.page_directory_pointer = virtual_address & 0x1ff;
    return indices;
}

struct page_table *PML4 = NULL;

void * V2P(void *virtual) {
    struct page_indices indices = get_page_indices((uint64_t) virtual);
    uint64_t *PDE; // page directory entry

    PDE = &PML4->entries[indices.page_directory_pointer];
    if (!get_flag(PDE, PRESENT))
        return NULL;
    struct page_table *PDP = (struct page_table *) (get_address(*PDE) << 12);

    PDE = &PDP->entries[indices.page_directory];
    if (!get_flag(PDE, PRESENT))
        return NULL;
    struct page_table *PD = (struct page_table *) (get_address(*PDE) << 12);

    PDE = &PD->entries[indices.page_table];
    if (!get_flag(PDE, PRESENT))
        return NULL;
    struct page_table *PT = (struct page_table *) (get_address(*PDE) << 12);

    PDE = &PT->entries[indices.page];
    // TODO: the correct one is physical2
    // uint64_t *physical = get_address(*PDE) << 12;
    uint64_t *physical2 = (void *)((*PDE & ~0xFFF) + ((unsigned long)virtual & 0xFFF));
    return physical2;
}

void map_memory(uint64_t virtual_memory, uint64_t physical_memory) {
    struct page_indices indices = get_page_indices(virtual_memory);
    uint64_t *PDE; // page directory entry

    PDE = &PML4->entries[indices.page_directory_pointer];
    struct page_table *PDP;
    if (!get_flag(PDE, PRESENT)) {
        PDP = request_page();
        *PDP = (struct page_table) {};
        set_address(PDE, (uint64_t) PDP >> 12);
        set_flag(PDE, PRESENT, 1);
        set_flag(PDE, READ_WRITE, 1);
    } else {
        PDP = (struct page_table *) (get_address(*PDE) << 12);
    }

    PDE = &PDP->entries[indices.page_directory];
    struct page_table *PD;
    if (!get_flag(PDE, PRESENT)) {
        PD = request_page();
        *PD = (struct page_table) {};
        set_address(PDE, (uint64_t) PD >> 12);
        set_flag(PDE, PRESENT, 1);
        set_flag(PDE, READ_WRITE, 1);
    } else {
        PD = (struct page_table *) (get_address(*PDE) << 12);
    }

    PDE = &PD->entries[indices.page_table];
    struct page_table *PT;
    if (!get_flag(PDE, PRESENT)) {
        PT = request_page();
        *PT = (struct page_table) {};
        set_address(PDE, (uint64_t) PT >> 12);
        set_flag(PDE, PRESENT, 1);
        set_flag(PDE, READ_WRITE, 1);
    } else {
        PT = (struct page_table *) (get_address(*PDE) << 12);
    }

    PDE = &PT->entries[indices.page];
    set_address(PDE, physical_memory >> 12);
    set_flag(PDE, PRESENT, 1);
    set_flag(PDE, READ_WRITE, 1);
}
