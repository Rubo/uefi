#ifndef BOOTINFO
#define BOOTINFO

#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>

#include "UEFI.h"

#define KERNEL_BASE 0xFFFFFFFF80000000
#define PAGE_SIZE 4096

struct memory_map {
    EFI_MEMORY_DESCRIPTOR *address;
    UINTN  size;
    UINTN  descriptor_size;
    UINT32 descriptor_version;
	UINTN  key;
};

struct framebuffer {
    void    *address;
    uint32_t size;
    uint32_t width;
    uint32_t height;
    uint32_t stride; // pixels per scanline
};

#define PSF1_MAGIC0 0x36
#define PSF1_MAGIC1 0x04

struct PSF1_header {
    uint8_t magic[2];
    uint8_t mode;
    uint8_t char_size;
};

struct PSF1_font {
    struct PSF1_header header;
    uint8_t *glyph_buffer;
};

struct RSDP {
    char     Signature[8]; // must be "RSD PTR "
    uint8_t  Checksum;
    char     OEMID[6];
    uint8_t  Revision;
    uint32_t RsdtAddress;

    // fields added in ACPI 2.0, valid when Revision >= 2
    uint32_t Length;
    uint64_t XsdtAddress;
    uint8_t  ExtendedChecksum;
    uint8_t  Reserved[3];
} __attribute__((packed));

struct boot_info {
    struct memory_map  memory_map;
    struct framebuffer framebuffer;
    struct PSF1_font   font;
    struct RSDP       *RSDP; // root system description pointer
    struct page_table *PML4;
};

#endif
