#include "common.h"
#include "memory.h"
#include "time.h"

double time_since_boot = 0;
uint64_t base_frequency = 1193182;
uint16_t divisor = 65535;

void tick(void) {
    time_since_boot += divisor / (double) base_frequency;
}

void sleepd(double seconds) {
    double start_time = time_since_boot;
    while (time_since_boot < start_time + seconds) {
        __asm__("hlt");
    }
}

void sleep(uint64_t milliseconds) {
    sleepd((double) milliseconds / 1000);
}

// PIT

#define PIT_COUNTER0 0x40
#define PIT_CMD 0x43

// BCD
#define CMD_BINARY 0x00 // Use Binary counter values
#define CMD_BCD    0x01 // Use Binary Coded Decimal counter values

// Mode
#define CMD_MODE0 0x00 // Interrupt on Terminal Count
#define CMD_MODE1 0x02 // Hardware Retriggerable One-Shot
#define CMD_MODE2 0x04 // Rate Generator
#define CMD_MODE3 0x06 // Square Wave
#define CMD_MODE4 0x08 // Software Trigerred Strobe
#define CMD_MODE5 0x0a // Hardware Trigerred Strobe

// Read/Write
#define CMD_LATCH   0x00
#define CMD_RW_LOW  0x10 // Least Significant Byte
#define CMD_RW_HI   0x20 // Most Significant Byte
#define CMD_RW_BOTH 0x30 // Least followed by Most Significant Byte

// Counter Select
#define CMD_COUNTER0 0x00
#define CMD_COUNTER1 0x40
#define CMD_COUNTER2 0x80
#define CMD_READBACK 0xc0

void set_divisor(uint16_t divisor) {
    if (divisor < 100) {
        divisor = 100;
    }
    outb(PIT_CMD, CMD_BINARY | CMD_MODE3 | CMD_RW_BOTH | CMD_COUNTER0);
    outb(PIT_COUNTER0, divisor & 0xff);
    outb(PIT_COUNTER0, (divisor & 0xff00) >> 8);
}
