// #include "common.h"

struct configuration_space {
    uint64_t base_address;
    uint16_t PCI_segment_group;
    uint8_t  start_bus;
    uint8_t  end_bus;
    uint32_t reserved;
} __attribute__((packed));


void enumerate_PCI(size_t count, struct configuration_space entries[count]);
