/* Local APIC registers */
#define LAPIC_ID    0x20  // Local APIC ID
#define LAPIC_TPR   0x80  // Task priority
#define LAPIC_EOI   0xB0  // End of interrupt
#define LAPIC_LDR   0xD0  // Logical destination
#define LAPIC_DFR   0xE0  // Destination format
#define LAPIC_SVR   0xF0  // Spurious vector
#define LAPIC_ICRLO 0x300 // Interrupt command (low)
#define LAPIC_ICRHI 0x310 // Interrupt command (high)

#define INT_SPURIOUS 255  // first four bits are set to 1 by the LAPIC_SVR

extern uint32_t lapic_addr;
extern uint8_t LAPIC_IDs[255]; // requires x2APIC to support more than 256 cores
extern uint8_t num_cores;

#define lapic_mminl(register) mminl(lapic_addr + register)
#define lapic_mmoutl(register, data) mmoutl(lapic_addr + register, data)

extern uint8_t  ioapic_id;
extern uint32_t ioapic_addr;

struct interrupt_frame;

extern uint32_t interrupt_overrides[256];

void prepare_interrupts(void);

__attribute__((interrupt)) void handle_page_fault(struct interrupt_frame *frame);
__attribute__((interrupt)) void handle_double_fault(struct interrupt_frame *frame);
__attribute__((interrupt)) void handle_general_protection_fault(struct interrupt_frame *frame);
__attribute__((interrupt)) void handle_spurious_interrupt(struct interrupt_frame *frame);
__attribute__((interrupt)) void handle_PIT(struct interrupt_frame *frame);
__attribute__((interrupt)) void handle_IPI(struct interrupt_frame *frame);
__attribute__((interrupt)) void handle_keyboard(struct interrupt_frame *frame);
