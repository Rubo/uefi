struct page_table {
    uint64_t entries[512];
} __attribute__((aligned(PAGE_SIZE)));

extern struct page_table *PML4;

void outb(uint32_t port, uint8_t data);
void outw(uint32_t port, uint16_t data);
void outl(uint32_t port, uint32_t data);
uint8_t  inb(uint32_t port);
uint16_t inw(uint32_t port);
uint32_t inl(uint32_t port);
void wait_IO(void);
void mmoutb(uintptr_t p, uint8_t data);
void mmoutw(uintptr_t p, uint16_t data);
void mmoutl(uintptr_t p, uint32_t data);
void mmoutll(uintptr_t p, uint64_t data);
uint8_t  mminb(uintptr_t p);
uint16_t mminw(uintptr_t p);
uint32_t mminl(uintptr_t p);
uint64_t mminll(uintptr_t p);
void     mmout(void *dst, const volatile void *src, size_t length);

void map_memory(uint64_t virtual_memory, uint64_t physical_memory);
void read_memory_map(struct memory_map memory_map);

void lock_pages(void *address, size_t count);
void free_pages(void *address, size_t count);
void reserve_pages(void *address, size_t count);
void unreserve_pages(void *address, size_t count);
void * request_page(void);

#define lock_page(address) lock_pages(address, 1)
#define free_page(address) free_pages(address, 1)
#define reserve_page(address) reserve_pages(address, 1)
#define unreserve_page(address) unreserve_pages(address, 1)
