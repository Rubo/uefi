#include "common.h"
#include "console.h"
#include "libc.h"
#include "memory.h"
#include "pci.h"

struct PCI_device_header {
    uint16_t vendor_ID;
    uint16_t device_ID;
    uint16_t command;
    uint16_t status;
    uint8_t  revision_ID;
    uint8_t  program_interface;
    uint8_t  subclass;
    uint8_t  class;
    uint8_t  cache_line_size;
    uint8_t  latency_timer;
    uint8_t  header_type;
    uint8_t  BIST;
} __attribute__((packed));

struct PCI_ID {
    uint64_t ID;
    char    *description;
};

enum PCI_vendor {
    AMD      = 0x1002,
    NVIDIA   = 0x10DE,
    REALTEK  = 0x10EC,
    _UNKNOWN = 0x1234,
    SAMSUNG  = 0x144D,
    QUALCOMM = 0x168C,
    ASMEDIA  = 0x1B21,
    RED_HAT  = 0x1B36,
    INTEL    = 0x8086,
};

struct PCI_ID vendors[] = {
    {AMD,      "Advanced Micro Devices, Inc. [AMD/ATI]"},
    {NVIDIA,   "NVIDIA Corporation"},
    {REALTEK,  "Realtek Semiconductor Co., Ltd."},
    {_UNKNOWN, "<unknown>"},
    {SAMSUNG,  "Samsung Electronics Co Ltd"},
    {QUALCOMM, "Qualcomm Atheros"},
    {ASMEDIA,  "ASMedia Technology Inc."},
    {RED_HAT, "Red Hat, Inc."},
    {INTEL,    "Intel Corp."},
    {0, NULL}
};

#define PCI_device(vendor, device) ((uint32_t) ((vendor) << 16) + (device))

struct PCI_ID devices[] = {
    {PCI_device(NVIDIA, 0x0FBA), "GM206 High Definition Audio Controller"},
    {PCI_device(NVIDIA, 0x1401), "GM206 [GeForce GTX 960]"},
    {PCI_device(NVIDIA, 0x1d11), "GP108M [GeForce MX230]"},
    {PCI_device(REALTEK, 0x8136), "RTL810xE PCI Express Fast Ethernet controller"},
    {PCI_device(SAMSUNG, 0xA809), "NVMe SSD Controller 980"},
    {PCI_device(QUALCOMM, 0x42), "QCA9377 802.11ac Wireless Network Adapter"},
    {PCI_device(ASMEDIA, 0x1080), "ASM1083/1085 PCIe to PCI Bridge"},
    {PCI_device(ASMEDIA, 0x1242), "ASM1142 USB 3.1 Host Controller"},
    {PCI_device(RED_HAT, 0x1), "QEMU PCI-PCI bridge"},
    {PCI_device(RED_HAT, 0x2), "QEMU PCI 16550A Adapter"},
    {PCI_device(RED_HAT, 0x3), "QEMU PCI Dual-port 16550A Adapter"},
    {PCI_device(RED_HAT, 0x4), "QEMU PCI Quad-port 16550A Adapter"},
    {PCI_device(RED_HAT, 0x5), "QEMU PCI Test Device"},
    {PCI_device(RED_HAT, 0x6), "PCI Rocker Ethernet switch device"},
    {PCI_device(RED_HAT, 0x7), "PCI SD Card Host Controller Interface"},
    {PCI_device(RED_HAT, 0x8), "QEMU PCIe Host bridge"},
    {PCI_device(RED_HAT, 0x9), "QEMU PCI Expander bridge"},
    {PCI_device(RED_HAT, 0xA), "PCI-PCI bridge (multiseat)"},
    {PCI_device(RED_HAT, 0xB), "QEMU PCIe Expander bridge"},
    {PCI_device(RED_HAT, 0xD), "QEMU XHCI Host Controller"},
    {PCI_device(RED_HAT, 0x100), "QXL paravirtual graphic card"},
    {PCI_device(INTEL, 0x15B8), "Ethernet Connection (2) I219-V"},
    {PCI_device(INTEL, 0x1901), "Xeon E3-1200 v5/E3-1500 v5/6th Gen Core Processor PCIe Controller (x16)"},
    {PCI_device(INTEL, 0x1911), "Xeon E3-1200 v5/v6 / E3-1500 v5 / 6th/7th Gen Core Processor Gaussian Mixture Model"},
    {PCI_device(INTEL, 0x191F), "Xeon E3-1200 v5/E3-1500 v5/6th Gen Core Processor Host Bridge/DRAM Registers"},
    {PCI_device(INTEL, 0x2918), "82801IB (ICH9) LPC Interface Controller"},
    {PCI_device(INTEL, 0x2922), "82801IR/IO/IH (ICH9R/DO/DH) 6 port SATA Controller [AHCI mode]"},
    {PCI_device(INTEL, 0x2930), "82801I (ICH9 Family) SMBus Controller"},
    {PCI_device(INTEL, 0x29C0), "82G33/G31/P35/P31 Express DRAM Controller"},
    {PCI_device(INTEL, 0x3482), "Ice Lake-LP LPC Controller"},
    {PCI_device(INTEL, 0x34A3), "Ice Lake-LP SMBus Controller"},
    {PCI_device(INTEL, 0x34A4), "Ice Lake-LP SPI Controller"},
    {PCI_device(INTEL, 0x34B0), "Ice Lake-LP PCI Express Root Port #9"},
    {PCI_device(INTEL, 0x34BC), "Ice Lake-LP PCI Express Root Port #5"},
    {PCI_device(INTEL, 0x34C5), "Ice Lake-LP Serial IO I2c Controller #4"},
    {PCI_device(INTEL, 0x34C8), "Smart Sound Technology Audio Controller"},
    {PCI_device(INTEL, 0x34D3), "Ice Lake-LP SATA Controller [AHCI mode]"},
    {PCI_device(INTEL, 0x34E0), "Management Engine Interface"},
    {PCI_device(INTEL, 0x34D5), "Ice Lake-LP Serial IO I2c Controller #4"},
    {PCI_device(INTEL, 0x34E8), "Ice Lake-LP Serial IO I2C Controller #0"},
    {PCI_device(INTEL, 0x34E9), "Ice Lake-LP Serial IO I2C Controller #1"},
    {PCI_device(INTEL, 0x34ED), "Ice Lake-LP USB 3.1 xHCI Host Controller"},
    {PCI_device(INTEL, 0x34EF), "Ice Lake-LP DRAM Controller"},
    {PCI_device(INTEL, 0x8A12), "Ice Lake-LP Processor Host Bridge/DRAM Registers"},
    {PCI_device(INTEL, 0x8A52), "Iris Plus Graphics G7"},
    {PCI_device(INTEL, 0xA102), "Q170/Q150/B150/H170/H110/Z170/CM236 Chipset SATA Controller [AHCI Mode]"},
    {PCI_device(INTEL, 0xA118), "100 Series/C230 Series Chipset Family PCI Express Root Port #9"},
    {PCI_device(INTEL, 0xA11A), "100 Series/C230 Series Chipset Family PCI Express Root Port #11"},
    {PCI_device(INTEL, 0xA11B), "100 Series/C230 Series Chipset Family PCI Express Root Port #12"},
    {PCI_device(INTEL, 0xA12F), "100 Series/C230 Series Chipset Family USB 3.0 xHCI Controller"},
    {PCI_device(INTEL, 0xA131), "100 Series/C230 Series Chipset Family Thermal Subsystem"},
    {PCI_device(INTEL, 0xA13A), "100 Series/C230 Series Chipset Family MEI Controller #1"},
    {PCI_device(INTEL, 0xA144), "H170 Chipset LPC/eSPI Controller"},
    {PCI_device(INTEL, 0xA121), "100 Series/C230 Series Chipset Family Power Management Controller"},
    {PCI_device(INTEL, 0xA170), "100 Series/C230 Series Chipset Family HD Audio Controller"},
    {PCI_device(INTEL, 0xA123), "100 Series/C230 Series Chipset Family SMBus"},
    {0, NULL}
};

enum PCI_class {
    UNCLASSIFIED                       = 0x0,
    MASS_STORAGE_CONTROLLER            = 0x1,
    NETWORK_CONTROLLER                 = 0x2,
    DISPLAY_CONTROLLER                 = 0x3,
    MULTIMEDIA_CONTROLLER              = 0x4,
    MEMORY_CONTROLLER                  = 0x5,
    BRIDGE                             = 0x6,
    SIMPLE_COMMUNICATION_CONTROLLER    = 0x7,
    BASE_SYSTEM_PERIPHERAL             = 0x8,
    INPUT_DEVICE_CONTROLLER            = 0x9,
    DOCKING_STATION                    = 0xA,
    PROCESSOR                          = 0xB,
    SERIAL_BUS_CONTROLLER              = 0xC,
    WIRELESS_CONTROLLER                = 0xD,
    INTELLIGENT_CONTROLLER             = 0xE,
    SATELLITE_COMMUNICATION_CONTROLLER = 0xF,
    ENCRYPTION_CONTROLLER              = 0x10,
    SIGNAL_PROCESSING_CONTROLLER       = 0x11,
    PROCESSING_ACCELERATOR             = 0x12,
    NON_ESSENTIAL_INSTRUMENTATION      = 0x13,
    CO_PROCESSOR                       = 0x40,
};

struct PCI_ID classes[] = {
    {UNCLASSIFIED, "Unclassified"},
    {MASS_STORAGE_CONTROLLER, "Mass Storage Controller"},
    {NETWORK_CONTROLLER, "Network Controller"},
    {DISPLAY_CONTROLLER, "Display Controller"},
    {MULTIMEDIA_CONTROLLER, "Multimedia Controller"},
    {MEMORY_CONTROLLER, "Memory Controller"},
    {BRIDGE, "Bridge Device"},
    {SIMPLE_COMMUNICATION_CONTROLLER, "Simple Communication Controller"},
    {BASE_SYSTEM_PERIPHERAL, "Base System Peripheral"},
    {INPUT_DEVICE_CONTROLLER, "Input Device Controller"},
    {DOCKING_STATION, "Docking Station"},
    {PROCESSOR, "Processor"},
    {SERIAL_BUS_CONTROLLER, "Serial Bus Controller"},
    {WIRELESS_CONTROLLER, "Wireless Controller"},
    {INTELLIGENT_CONTROLLER, "Intelligent Controller"},
    {SATELLITE_COMMUNICATION_CONTROLLER, "Satellite Communication Controller"},
    {ENCRYPTION_CONTROLLER, "Encryption Controller"},
    {SIGNAL_PROCESSING_CONTROLLER, "Signal Processing Controller"},
    {PROCESSING_ACCELERATOR, "Processing Accelerator"},
    {NON_ESSENTIAL_INSTRUMENTATION, "Non Essential Instrumentation"},
    {CO_PROCESSOR, "Co-Processor"},
    {0, NULL}
};

#define PCI_subclass(class, subclass) (((uint16_t) (class) << 8) + (subclass))

/* 0x1 - Mass Storage Controller */
#define SERIAL_ATA_CONTROLLER          0x6
#define NON_VOLATILE_MEMORY_CONTROLLER 0x8

/* 0x3 - Display Controller */
#define VGA_COMPATIBLE_CONTROLLER 0x0

/* 0x6 - Bridge */
#define PCI_TO_PCI_BRIDGE 0x4

/* 0xC - Serial Bus Controller */
#define USB_CONTROLLER 0x3

struct PCI_ID subclasses[] = {
    {PCI_subclass(MASS_STORAGE_CONTROLLER, SERIAL_ATA_CONTROLLER), "Serial ATA Controller"},
    {PCI_subclass(MASS_STORAGE_CONTROLLER, NON_VOLATILE_MEMORY_CONTROLLER), "Non-Volatile Memory Controller"},
    {PCI_subclass(NETWORK_CONTROLLER, 0x0), "Ethernet Controller"},
    {PCI_subclass(NETWORK_CONTROLLER, 0x80), "Other"},
    {PCI_subclass(DISPLAY_CONTROLLER, VGA_COMPATIBLE_CONTROLLER), "VGA Compatible Controller"},
    {PCI_subclass(DISPLAY_CONTROLLER, 0x2), "3D Controller (Not VGA-Compatible)"},
    {PCI_subclass(MULTIMEDIA_CONTROLLER, 0x3), "Audio Device"},
    {PCI_subclass(MEMORY_CONTROLLER, 0x0), "RAM Controller"},
    {PCI_subclass(MEMORY_CONTROLLER, 0x80), "Other"},
    {PCI_subclass(BRIDGE, 0x0), "Host Bridge"},
    {PCI_subclass(BRIDGE, 0x1), "ISA Bridge"},
    {PCI_subclass(BRIDGE, PCI_TO_PCI_BRIDGE), "PCI-to-PCI Bridge"},
    {PCI_subclass(SIMPLE_COMMUNICATION_CONTROLLER, 0x80), "Other"},
    {PCI_subclass(BASE_SYSTEM_PERIPHERAL, 0x80), "Other"},
    {PCI_subclass(SERIAL_BUS_CONTROLLER, USB_CONTROLLER), "USB Controller"},
    {PCI_subclass(SERIAL_BUS_CONTROLLER, 0x5), "SMBus Controller"},
    {PCI_subclass(SERIAL_BUS_CONTROLLER, 0x80), "Other"},
    {PCI_subclass(SIGNAL_PROCESSING_CONTROLLER, 0x80), "Other"},
    {0, NULL},
};

#define PCI_interface(class, subclass, interface) (((uint32_t) (class) << 16) + ((uint16_t) (subclass) << 8) + (interface))

struct PCI_ID interfaces[] = {
    {PCI_interface(MASS_STORAGE_CONTROLLER, SERIAL_ATA_CONTROLLER, 0x0), "Vendor Specific Interface"},
    {PCI_interface(MASS_STORAGE_CONTROLLER, SERIAL_ATA_CONTROLLER, 0x1), "AHCI 1.0"},
    {PCI_interface(MASS_STORAGE_CONTROLLER, SERIAL_ATA_CONTROLLER, 0x2), "Serial Storage Bus"},
    {PCI_interface(MASS_STORAGE_CONTROLLER, NON_VOLATILE_MEMORY_CONTROLLER, 0x1), "NVMHCI"},
    {PCI_interface(MASS_STORAGE_CONTROLLER, NON_VOLATILE_MEMORY_CONTROLLER, 0x2), "NVM Express"},
    {PCI_interface(DISPLAY_CONTROLLER, VGA_COMPATIBLE_CONTROLLER, 0x0), "VGA Controller"},
    {PCI_interface(DISPLAY_CONTROLLER, VGA_COMPATIBLE_CONTROLLER, 0x1), "8514-Compatible Controller"},
    {PCI_interface(BRIDGE, PCI_TO_PCI_BRIDGE, 0x0), "Normal Decode"},
    {PCI_interface(BRIDGE, PCI_TO_PCI_BRIDGE, 0x1), "Subtractive Decode"},
    {PCI_interface(SERIAL_BUS_CONTROLLER, USB_CONTROLLER, 0x0), "UHCI Controller"},
    {PCI_interface(SERIAL_BUS_CONTROLLER, USB_CONTROLLER, 0x10), "OHCI Controller"},
    {PCI_interface(SERIAL_BUS_CONTROLLER, USB_CONTROLLER, 0x20), "EHCI (USB2) Controller"},
    {PCI_interface(SERIAL_BUS_CONTROLLER, USB_CONTROLLER, 0x30), "XHCI (USB3) Controller"},
    {PCI_interface(SERIAL_BUS_CONTROLLER, USB_CONTROLLER, 0x80), "Unspecified"},
    {PCI_interface(SERIAL_BUS_CONTROLLER, USB_CONTROLLER, 0xFE), "USB Device (Not a host controller)"},
    {0, NULL},
};

char * get_name(struct PCI_ID *list, uint32_t ID) {
    for (int i = 0; list[i].description; i++) {
        if (list[i].ID == ID) {
            return list[i].description;
        }
    }
    return NULL;
}

void enumerate_function(uint64_t device_address, uint64_t function) {
    uint64_t offset = function << 12;
    uint64_t function_address = device_address + offset;
    map_memory(function_address, function_address);

    struct PCI_device_header *header = (void *) function_address;
    if (header->device_ID == 0 || header->device_ID == 0xFFFF) { // invalid ID
        return;
    }

    /*
     * TODO: it would be easier to do the lookup online, when some networking functionalities
     * are implemented.
     */
    char vendor_hex[64];
    char *vendor_name = get_name(vendors, header->vendor_ID);
    if (!vendor_name) {
        snprintf(vendor_hex, sizeof(vendor_hex), "0x%x", header->vendor_ID);
    }
    char device_hex[64];
    char *device_name = get_name(devices, PCI_device(header->vendor_ID, header->device_ID));
    if (!device_name) {
        snprintf(device_hex, sizeof(device_hex), "0x%x", header->device_ID);
    }

    char class_hex[64];
    char *class_name = get_name(classes, header->class);
    if (!class_name) {
        snprintf(class_hex, sizeof(class_hex), "0x%x", header->class);
    }

    char subclass_hex[64];
    char *subclass_name = get_name(subclasses, PCI_subclass(header->class, header->subclass));
    if (!subclass_name) {
        snprintf(subclass_hex, sizeof(subclass_hex), "0x%x", header->subclass);
    }

    char interface_hex[64];
    char *interface_name = get_name(interfaces, PCI_interface(header->class, header->subclass, header->program_interface));
    if (!interface_name) {
        snprintf(interface_hex, sizeof(interface_hex), "0x%x", header->program_interface);
    }

    char buf[512];
    snprintf(buf, sizeof(buf), "%s / %s / %s / %s / %s\n",
        vendor_name    ? vendor_name    : vendor_hex,
        device_name    ? device_name    : device_hex,
        class_name     ? class_name     : class_hex,
        subclass_name  ? subclass_name  : subclass_hex,
        interface_name ? interface_name : interface_hex);
    print(buf);
}

void enumerate_device(uint64_t bus_address, uint64_t device) {
    uint64_t offset = device << 15;
    uint64_t device_address = bus_address + offset;
    map_memory(device_address, device_address);

    struct PCI_device_header *header = (void *) device_address;
    if (header->device_ID == 0 || header->device_ID == 0xFFFF) { // invalid ID
        return;
    }
    /* there are 8 functions per device */
    for (uint64_t function = 0; function < 8; function++) {
        enumerate_function(device_address, function);
    }
}

void enumerate_bus(uint64_t base_address, uint64_t bus) {
    uint64_t offset = bus << 20;
    uint64_t bus_address = base_address + offset;
    map_memory(bus_address, bus_address);

    struct PCI_device_header *header = (void *) bus_address;
    if (header->device_ID == 0 || header->device_ID == 0xFFFF) { // invalid ID
        return;
    }
    /* there are 32 devices per bus */
    for (uint64_t device = 0; device < 32; device++) {
        enumerate_device(bus_address, device);
    }
}

void enumerate_PCI(size_t count, struct configuration_space entries[count]) {
    for (int i = 0; i < count; i++) {
        struct configuration_space entry = entries[i];
        for (uint64_t bus = entry.start_bus; bus < entry.end_bus; bus++) {
            enumerate_bus(entry.base_address, bus);
        }
    }
}
