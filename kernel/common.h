#ifndef COMMON_H
#define COMMON_H

#include "bootinfo.h"

#define arrlen(array) (sizeof(array) / sizeof(array[0]))
#define unused

void panic(char *message);

#endif
