#include "common.h"
#include "console.h"
#include "idt.h"
#include "libc.h"
#include "memory.h"
#include "pci.h"

struct SDT_header {
    char       Signature[4];
    uint32_t   Length;
    uint8_t    Revision;
    uint8_t    Checksum;
    uint8_t    OEMID[6];
    uint8_t    OEM_Table_ID[8];
    uint32_t   OEM_Revision;
    uint32_t   Creator_ID;
    uint32_t   Creator_Revision;
} __attribute__((packed));

struct XSDT {
    struct SDT_header  header;
    struct SDT_header *entries[];
} __attribute__((packed));

/* see https://wiki.osdev.org/PCI_Express */
struct MCFG {
    struct SDT_header          header;
    uint64_t                   reserved;
    struct configuration_space entries[];
} __attribute__((packed));

/* see https://wiki.osdev.org/MADT */
struct MADT {
    struct SDT_header header;
    uint32_t LocalInterruptControllerAddress;
    uint32_t Flags;
    uint8_t  InterruptControllerStructures[];
} __attribute__((packed));

/* MADT flags */
#define PCAT_COMPAT 0b1 // the system also has a PC-AT-compatible dual-8259 setup; the 8259 vectors must be disabled (i.e. masked)

/* see ACPI specification, release 6.5, table 5.21 */
enum interrupt_controllers {
    PROCESSOR_LOCAL_APIC = 0,
    IO_APIC,
    INTERRUPT_SOURCE_OVERRIDE,
    NMI_SOURCE, // non-maskable interrupt
    LOCAL_APIC_NMI,
    LOCAL_APIC_ADDRESS_OVERRIDE,
    IO_SAPIC,
    LOCAL_SAPIC,
    PLATFORM_INTERRUPT_SOURCES,
    PROCESSOR_LOCAL_X2APIC,
    LOCAL_X2APIC_NMI,
    GICC, // GIC CPU interface
    GICD, // GIC distributor
    GIC_MSI_FRAME,
    GICR, // GIC redistributor
    GIC_ITS, // interrupt translation service
    MULTIPROCESSOR_WAKEUP,
    CORE_PIC, // core programmable interrupt controller
    LIO_PIC,  // legacy I/O programmable interrupt controller
    HT_PIC,   // HyperTransport programmable interrupt controller
    EIO_PIC,  // extend I/O programmable interrupt controller
    MSI_PIC,  // MSI programmable interrupt controller
    BIO_PIC,  // bridge I/O programmable interrupt controller
    LPC_PIC,  // low pin count programmable interrupt controller
    /* 0x18 - 0x7f: reserved */
    /* 0x80 - 0xff: reserved for OEM use */
};

struct interrupt_controller {
    uint8_t type;   // one of the values of the enum interrupt_controllers
    uint8_t length;
} __attribute__((packed));

#define ENABLED 0b1
#define ONLINE_CAPABLE 0b10

struct LAPIC {
    struct interrupt_controller header; // length must be 8
    uint8_t  ACPI_Processor_UID;
    uint8_t  ACPI_ID;
    uint32_t Flags;
} __attribute__((packed));

struct IOAPIC {
    struct interrupt_controller header; // length must be 12
    uint8_t IOAPIC_ID;
    uint8_t Reserved;
    uint32_t IOAPIC_Address;
    uint32_t GlobalSystemInterruptBase;
} __attribute__((packed));

struct InterruptSourceOverride {
    struct interrupt_controller header; // length must be 10
    uint8_t Bus;
    uint8_t Source;
    uint32_t GlobalSystemInterrupt;
    uint16_t Flags;
} __attribute__((packed));

struct MPWMailbox {
    uint16_t Command;
    uint16_t Reserved;
    uint32_t ApicId;
    void (*WakeupVector)();
    uint8_t ReservedForOs[2032];
    uint8_t ReservedForFirmware[2048];
} __attribute__((packed));

struct MultiprocessorWakeup {
    struct interrupt_controller header; // length must be 16
    uint16_t MailBoxVersion;
    uint32_t Reserved;
    struct MPWMailbox *MailBoxAddress;
} __attribute__((packed));

void prepare_ACPI(struct RSDP *RSDP) {
    /* RSDP validation */
    uint8_t checksum = 0;
    uint8_t *bytes = (uint8_t *) RSDP;
    for (int i = 0; i < sizeof(*RSDP); i++) {
        checksum += bytes[i];
    }
    if (checksum) {
        print("RSDP is not valid\n");
        return;
    }

    struct XSDT *XSDT = (struct XSDT *) RSDP->XsdtAddress;

    /* the entries array size is n pointers, in this way we find n */
    size_t entries_count = (XSDT->header.Length - sizeof(XSDT->header)) / sizeof(void *);
    /* after the header, there is an array of pointers to other headers */
    for (int i = 0; i < entries_count; i++) {
        struct SDT_header *entry_header = XSDT->entries[i];
        for (int j = 0; j < arrlen(entry_header->Signature); j++) {
            putchar(entry_header->Signature[j]);
        }
        putchar(' ');
    }
    putchar('\n');

    for (int i = 0; i < entries_count; i++) {
        struct SDT_header *entry_header = XSDT->entries[i];

        if (!strncmp("MCFG", entry_header->Signature, arrlen(entry_header->Signature))) {
            struct MCFG *MCFG = (void *) entry_header;
            /* sizeof(*MCFG) doesn't take into account the variable length array */
            size_t entries_count = (MCFG->header.Length - sizeof(*MCFG)) / sizeof(struct configuration_space);
            enumerate_PCI(entries_count, MCFG->entries);
        } else if (!strncmp("APIC", entry_header->Signature, arrlen(entry_header->Signature))) {
            struct MADT *MADT = (void *) entry_header;

            lapic_addr = MADT->LocalInterruptControllerAddress;
            map_memory(lapic_addr, lapic_addr);

            uint8_t *table = MADT->InterruptControllerStructures;
            uint8_t *end = (uint8_t *) MADT + MADT->header.Length;

            print("MADT interrupt controller structure types:");
            for (; table < end; table += table[1]) {
                char buf[16];
                snprintf(buf, sizeof(buf), " 0x%x", table[0]);
                print(buf);
            }
            putchar('\n');
            table = MADT->InterruptControllerStructures;

            for (; table < end; table += table[1]) {
                if (table[0] == PROCESSOR_LOCAL_APIC && table[4] | 0b11) {
                    LAPIC_IDs[num_cores++] = table[3];
                } else if (table[0] == IO_APIC) {
                    struct IOAPIC *ioapic = (void *) table;
                    ioapic_id = ioapic->IOAPIC_ID;
                    ioapic_addr = ioapic->IOAPIC_Address;
                    map_memory(ioapic_addr, ioapic_addr);
                    char buf[256];
                    snprintf(buf, sizeof(buf), "I/O APIC ID = %u\n", ioapic_id);
                    print(buf);
                } else if (table[0] == INTERRUPT_SOURCE_OVERRIDE) {
                    struct InterruptSourceOverride *override = (void *) table;
                    interrupt_overrides[override->Source] = override->GlobalSystemInterrupt;
                    char buf[256];
                    snprintf(buf, sizeof(buf), "Remapped ISA IRQ %u to APIC INT %u\n", override->Source, override->GlobalSystemInterrupt);
                    print(buf);
                } else if (table[0] == MULTIPROCESSOR_WAKEUP) {
                    /* not implemented */
                    struct MultiprocessorWakeup *mpw = (void *) table;
                    char buf[256];
                    snprintf(buf, sizeof(buf), "Multiprocessor wakeup mailbox address: 0x%x\n", mpw->MailBoxAddress);
                    print(buf);
                }
            }
            char buf[256];
            snprintf(buf, sizeof(buf), "Local processors found: %u\n", num_cores);
            print(buf);
        }
    }
}
