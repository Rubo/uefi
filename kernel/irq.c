/*
 * From https://github.com/dreamos82/Osdev-Notes/blob/master/02_Architecture/05_InterruptHandling.md
 *
 * Using __attribute__((interrupt)) may seem tempting with how simple it is, and it lets you avoid
 * assembly! This is easy mistake to make (one I made myself early on). This method is best avoided
 * as covers the simple case of saving all the general purpose registers, but does nothing else.
 * Later on you will want to do other things inside your interrupt stub, and thus have to abandon
 * the attribute and write your own stub anyway. Better to get it right from the beginning.
 *  - DT.
 */
#include "common.h"
#include "console.h"
#include "idt.h"
#include "memory.h"
#include "time.h"

const char ASCII[] = {
     0 ,  0 , '1', '2',
    '3', '4', '5', '6',
    '7', '8', '9', '0',
    '-', '=',  0 ,  0 ,
    'q', 'w', 'e', 'r',
    't', 'y', 'u', 'i',
    'o', 'p', '[', ']',
     0 ,  0 , 'a', 's',
    'd', 'f', 'g', 'h',
    'j', 'k', 'l', ';',
    '\'','`',  0 , '\\',
    'z', 'x', 'c', 'v',
    'b', 'n', 'm', ',',
    '.', '/',  0 , '*',
     0 , ' '
};

char translate(uint8_t scancode, bool uppercase) {
    if (scancode > 58)
        return 0;
    char result = ASCII[scancode];
    if (uppercase) {
        result -= 32;
    }
    return result;
}

/* we can't recover, basically we can only abort */
__attribute__((interrupt))
void handle_double_fault(struct interrupt_frame *frame) {
    panic("double fault detected");
}

__attribute__((interrupt))
void handle_general_protection_fault(struct interrupt_frame *frame) {
    panic("general protection fault detected");
}


__attribute__((interrupt))
void handle_page_fault(struct interrupt_frame *frame) {
    panic("page fault detected");
}

__attribute__((interrupt))
void handle_spurious_interrupt(struct interrupt_frame *frame) {
    print("spurious interrupt\n");
    lapic_mmoutl(LAPIC_EOI, 0);
}

// PIC

#define LEFT_SHIFT 0x2a
#define RIGHT_SHIFT 0x36
#define ENTER 0x1c
#define BACKSPACE 0x0e
#define SPACEBAR 0x39

bool is_left_shift_pressed = false;
bool is_right_shift_pressed = false;

__attribute__((interrupt))
void handle_keyboard(struct interrupt_frame *frame) {
    /* assume QWERTY-US keyboard */
    uint8_t scancode = inb(0x60);

    switch(scancode) {
    case LEFT_SHIFT:
        is_left_shift_pressed = true;
        goto end;
    case LEFT_SHIFT + 0x80:
        is_left_shift_pressed = false;
        goto end;
    case RIGHT_SHIFT:
        is_right_shift_pressed = true;
        goto end;
    case RIGHT_SHIFT + 0x80:
        is_right_shift_pressed = false;
        goto end;
    case ENTER:
        renderer.cursor.x = 0;
        renderer.cursor.y += 16;
        goto end;
    case BACKSPACE:
        ungetc();
        goto end;
    case SPACEBAR:
        putchar(' ');
        goto end;
    }

    char ascii = translate(scancode, is_left_shift_pressed | is_right_shift_pressed);
    if (ascii != 0) {
        putchar(ascii);
    }

end:
    lapic_mmoutl(LAPIC_EOI, 0);
}

__attribute__((interrupt))
void handle_PIT(struct interrupt_frame *frame) {
    tick();
    lapic_mmoutl(LAPIC_EOI, 0);
}

__attribute__((interrupt))
void handle_IPI(struct interrupt_frame *frame) {
    print("IPI received\n");
    lapic_mmoutl(LAPIC_EOI, 0);
}
