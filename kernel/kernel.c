#include "common.h"
#include "acpi.h"
#include "console.h"
#include "idt.h"
#include "libc.h"
#include "memory.h"
#include "time.h"

struct GDT_descriptor {
    uint16_t size;
    uint64_t offset;
} __attribute__((packed));

struct GDT_entry {
    uint16_t limit0;
    uint16_t base0;
    uint8_t base1;
    uint8_t access_byte;
    uint8_t limit1_flags; // 4 bits for limit1, 4 bits for flags
    uint8_t base2;
} __attribute__((packed));

struct GDT {
    struct GDT_entry null;        // segment 0x00
    struct GDT_entry kernel_code; // segment 0x08
    struct GDT_entry kernel_data; // segment 0x10
    struct GDT_entry user_code;
    struct GDT_entry user_data;
} __attribute__((packed)) __attribute__((aligned(PAGE_SIZE)));

void load_GDT(struct GDT_descriptor *descriptor); // defined in gdt.asm

__attribute__((aligned(PAGE_SIZE)))
struct GDT GDT = {
    .kernel_code = {0, 0, 0, 0x9a, 0xa0, 0},
    .kernel_data = {0, 0, 0, 0x92, 0xa0, 0},
    .user_code = {0, 0, 0, 0x9a, 0xa0, 0},
    .user_data = {0, 0, 0, 0x92, 0xa0, 0},
};

extern uint64_t kernel_start;
extern uint64_t kernel_end;

void panic(char *message) {
    char buf[strlen(message) + 16];
    snprintf(buf, sizeof(buf), "Kernel panic: %s\n", message);
    print(buf);
    while (true) {};
}

// int ap_done = 0;
// int bspdone = 0;
// extern void ap_trampoline();
// void f() {
//     print("done\n");
// }
/**
 * Initialize logical cores
 * Because Local APIC ID is not contiguous, core id != core num
 */
// void bootboot_startcore(void) {
//     print("done\n");
    // (void)buf;
    // register UINT16 core_num = 0;
    // if(lapic_addr) {
    //     // enable Local APIC
    //     *((volatile uint32_t*)(lapic_addr + 0x0F0)) = *((volatile uint32_t*)(lapic_addr + 0x0F0)) | 0x100;
    //     core_num = LAPIC_IDs[*((volatile uint32_t*)(lapic_addr + 0x20)) >> 24];
    // }

    // ap_done = 1;

    // // spinlock until BSP finishes (or forever if we got an invalid lapicid, should never happen)
    // do { __asm__ __volatile__ ("pause" : : : "memory"); } while(!bspdone);

    // // enable SSE
    // __asm__ __volatile__ (
    //     "movl $0xC0000011, %%eax;"
    //     "movq %%rax, %%cr0;"
    //     "movq %%cr4, %%rax;"
    //     "orw $3 << 8, %%ax;"
    //     "mov %%rax, %%cr4"
    //     : );

    // // set up paging
    // __asm__ __volatile__ (
    //     "mov %0, %%rax;"
    //     "mov %%rax, %%cr3"
    //     : : "b"(PML4) : "memory" );

    // // set stack and call _start() in sys/core
    // UINT64 initstack = 1024;
    // __asm__ __volatile__ (
    //     // get a valid stack for the core we're running on
    //     "xorq %%rsp, %%rsp;"
    //     "subq %0, %%rsp;"  // sp = core_num * -initstack
    //     // pass control over
    //     "pushq %1;"
    //     "movq %2, %%rdi;"
    //     "retq"
    //     : : "a"((UINTN)core_num*initstack), "b"(f), "c"((UINTN)core_num) : "memory" );
// }


void main(struct boot_info *boot_info) {
    PML4 = boot_info->PML4;

    renderer.framebuffer = &boot_info->framebuffer;
    renderer.psf1 = boot_info->font;

    print("\n\n\n\n\n\n\n\n\n\n\n\n\n\n\nThis is Major Tom to ground control, I'm stepping through the door\n");

    struct GDT_descriptor descriptor = {
        .size = sizeof(struct GDT) - 1, // 0xFFF
        .offset = (uint64_t) &GDT,
    };
    load_GDT(&descriptor);

    read_memory_map(boot_info->memory_map);

    /*
     * the bootloader should already have marked the kernel pages,
     * but we want to be 100% sure they are locked
     */
    uint64_t kernel_size  = &kernel_end - &kernel_start;
    uint64_t kernel_pages = kernel_size / PAGE_SIZE + 1; // the kernel occupies at least 1 page
    lock_pages(&kernel_start, kernel_pages);

    prepare_ACPI(boot_info->RSDP);

    prepare_interrupts();

    /*
     * Sources for symmetric multi-processing:
     * - https://wiki.osdev.org/SMP
     * - https://wiki.osdev.org/Entering_Long_Mode_Directly
     * - https://www.osdever.net/tutorials/view/multiprocessing-support-for-hobby-oses-explained
     * - https://gitlab.com/bztsrc/bootboot/-/blob/master/x86_64-efi/bootboot.c
     */

    // int aprunning = 0;
    // memcpy((void *) 0x8000, &ap_trampoline, PAGE_SIZE);

    // map_memory(0x8000, 0x8000);

    // copy trampoline and save UEFI's 64 bit system registers for the trampoline code
    // __asm__ __volatile__ (
    //     "movq $32, %%rcx; movq %0, %%rsi; movq $0x8000, %%rdi; repnz movsq;"
    //     "movq %%cr3, %%rax; movq %%rax, 0x80C0;"
    //     "movl %%cs, %%eax; movl %%eax, 0x80CC;"
    //     "movl %%ds, %%eax; movl %%eax, 0x80D0;"
    //     "movq %%rbx, 0x80D8;"
    //     "sgdt 0x80E0;" : : "d"((uint64_t)&ap_trampoline), "b"((uint64_t)&bootboot_startcore) :
    // );

    /*
     * NOTE: this should not be needed when I get access to a firmware which supports ACPI 6.4+,
     * as it introduced the Multiprocessor Wakeup structure.
     */
    // #define ICR_INIT 0x500
    // #define ICR_STARTUP 0x600
    // #define ICR_DELIVERY_SEND_PENDING 0x1000
    // #define ICR_ASSERT 0x4000
    // #define ICR_DESTINATION_SHIFT 24

    // #define send_ipi(a,m,v) \
    //     do { \
    //         while(*((volatile uint32_t*)(lapic_addr + 0x300)) & (1 << 12)) __asm__ __volatile__ ("pause" : : : "memory"); \
    //         *((volatile uint32_t*)(lapic_addr + 0x310)) = (*((volatile uint32_t*)(lapic_addr + 0x310)) & 0x00ffffff) | (a << 24); \
    //         *((volatile uint32_t*)(lapic_addr + 0x300)) = (*((volatile uint32_t*)(lapic_addr + 0x300)) & m) | v;  \
    //     } while(0)

    // for (uint8_t i = 0; i < num_cores; i++) {
    //     if (LAPIC_IDs[i] == lapic_mminl(LAPIC_ID) >> 24) {
    //         continue;
    //     }
        // lapic_mmoutl(LAPIC_ICRHI, LAPIC_IDs[i] << ICR_DESTINATION_SHIFT);
        // lapic_mmoutl(LAPIC_ICRLO, ICR_INIT | ICR_ASSERT);
        // while(lapic_mminl(LAPIC_ICRLO) & ICR_DELIVERY_SEND_PENDING) {
        //     __asm__ __volatile__("hlt");
        // }

        // *((volatile uint32_t*)(lapic_addr + 0x280)) = 0;        // clear APIC errors
        // uint32_t a = *((volatile uint32_t*)(lapic_addr + 0x280));
        // send_ipi(i, 0xfff00000, 0x00C500);                      // trigger INIT IPI
        // sleep(1);
        // send_ipi(i, 0xfff00000, 0x008500);                      // deassert INIT IPI

        // break;
    // }

    // sleep(10);

    for (uint8_t i = 0; i < num_cores; i++) {
        if (LAPIC_IDs[i] == lapic_mminl(LAPIC_ID) >> 24) {
            continue;
        }
        // lapic_mmoutl(LAPIC_ICRHI, LAPIC_IDs[i] << ICR_DESTINATION_SHIFT);
        // lapic_mmoutl(LAPIC_ICRLO, 0x8 | ICR_STARTUP | ICR_ASSERT);
        // while(lapic_mminl(LAPIC_ICRLO) & ICR_DELIVERY_SEND_PENDING) {
        //     __asm__ __volatile__("hlt");
        // }

        // ap_done = 0;
        // send_ipi(i, 0xfff0f800, 0x004608);                      // trigger SIPI, start at 0800:0000h
        // for(uint32_t a = 250; !ap_done && a > 0; a--) sleep(1);          // wait for AP with 50 msec timeout
        // if(!ap_done) {
        //     send_ipi(i, 0xfff0f800, 0x004608);
        //     sleep(250);
        // }
        // break;
    }

    // bspdone = 1;
    // char buf[256];
    // snprintf(buf, sizeof(buf), "%u/%u application processors have been initialised\n", aprunning, num_cores);
    // print(buf);

    while (1) {
        __asm__ __volatile__("hlt");
    };
}
