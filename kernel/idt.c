#include "common.h"
#include "idt.h"
#include "memory.h"

#define IDT_TA_InterruptGate 0b10001110
#define IDT_TA_CallGate      0b10001100
#define IDT_TA_TrapGate      0b10001111

struct IDT_descriptor_entry {
    uint16_t offset0;
    uint16_t segment_selector; // when the interrupt is caught, the CPU will switch to this segment
    uint8_t  IST_offset; // interrupt stack table
    uint8_t  type_attrs; // type and attributes
    uint16_t offset1;
    uint32_t offset2;
    uint32_t ignored0;
};

struct IDTR {
    int16_t limit;
    int64_t offset;
} __attribute__((packed));

uint32_t lapic_addr;
uint8_t LAPIC_IDs[255] = {};
uint8_t num_cores = 0;

/* memory-mapped I/O APIC registers */
#define IOREGSEL 0x00
#define IOWIN    0x10

/* I/O APIC registers */
#define IOAPICID  0x00
#define IOAPICVER 0x01
#define IOAPICARB 0x02
#define IOREDTBL  0x10

uint8_t  ioapic_id;
uint32_t ioapic_addr;

static uint32_t ioapic_in(uint8_t reg) {
    mmoutl(ioapic_addr + IOREGSEL, reg);
    uint32_t result = mminl(ioapic_addr + IOWIN);
    return result;
}

static void ioapic_out(uint8_t reg, uint8_t data) {
    mmoutl(ioapic_addr + IOREGSEL, reg);
    mmoutl(ioapic_addr + IOWIN, data);
}

static void set_entry(uint8_t index, uint64_t data) {
    ioapic_out(IOREDTBL + index * 2, (uint32_t) data);
    ioapic_out(IOREDTBL + index * 2 + 1, (uint32_t) (data >> 32));
}

struct IDT_descriptor_entry interrupts[256];
uint32_t interrupt_overrides[256] = {};

/* assuming no IRQ gets remapped to INT 0 */
#define remap(IRQ) ((interrupt_overrides[IRQ]) ? (interrupt_overrides[IRQ]) : IRQ)

void set_offset(struct IDT_descriptor_entry *entry, uint64_t offset) {
    entry->offset0 = (uint16_t) (offset & 0xffff);
    entry->offset1 = (uint16_t) ((offset & 0xffff0000) >> 16);
    entry->offset2 = (uint32_t) ((offset & 0xffffffff00000000) >> 32);
}

uint64_t get_offset(struct IDT_descriptor_entry *entry) {
    uint64_t offset = 0;
    offset |= (uint64_t) entry->offset0;
    offset |= (uint64_t) entry->offset1 << 16;
    offset |= (uint64_t) entry->offset2 << 32;
    return offset;
}

void set_IDT_gate(void (*handler)(struct interrupt_frame *), uint8_t entry_offset, uint8_t type_attrs, uint8_t selector) {
    struct IDT_descriptor_entry *interrupt = &interrupts[entry_offset];
    set_offset(interrupt, (uint64_t) handler);
    interrupt->type_attrs = type_attrs;
    interrupt->segment_selector = selector;
}

// PIC1 is the master, PIC2 is the slave
#define PIC_MASTER_COMMAND 0x20
#define PIC_MASTER_DATA 0x21
#define PIC_SLAVE_COMMAND 0xA0
#define PIC_SLAVE_DATA 0xA1
#define PIC_EOI 0x20 // end of interrupt

#define ICW1_INIT 0x10
#define ICW1_ICW4 0x01
#define ICW4_8086 0x01

void disable_PIC(void) {
    outb(PIC_MASTER_COMMAND, ICW1_INIT | ICW1_ICW4);
    outb(PIC_SLAVE_COMMAND, ICW1_INIT | ICW1_ICW4);

    outb(PIC_MASTER_DATA, 0x20);
    outb(PIC_SLAVE_DATA, 0x28);

    outb(PIC_MASTER_DATA, 0x2);
    outb(PIC_SLAVE_DATA, 0x4);

    outb(PIC_MASTER_DATA, ICW4_8086);
    outb(PIC_SLAVE_DATA, ICW4_8086);

    outb(PIC_MASTER_DATA, 0xFF);
    outb(PIC_SLAVE_DATA, 0xFF);
}

void prepare_interrupts(void) {
    disable_PIC();

    /* disable all interrupt vectors */
    for (int i = 0; i < arrlen(interrupts); i++) {
        set_IDT_gate(NULL, 0, 0, 0);
    }

    #define INT_TIMER 0x20
    #define INT_KEYBOARD 0x21
    set_IDT_gate(handle_double_fault, 0x8, IDT_TA_InterruptGate, 0x08);
    set_IDT_gate(handle_general_protection_fault, 0xD, IDT_TA_InterruptGate, 0x08);
    set_IDT_gate(handle_double_fault, 0xE, IDT_TA_InterruptGate, 0x08);
    set_IDT_gate(handle_PIT, INT_TIMER, IDT_TA_InterruptGate, 0x08);
    set_IDT_gate(handle_keyboard, INT_KEYBOARD, IDT_TA_InterruptGate, 0x08);
    set_IDT_gate(handle_IPI, 254, IDT_TA_InterruptGate, 0x08);
    set_IDT_gate(handle_spurious_interrupt, INT_SPURIOUS, IDT_TA_InterruptGate, 0x08);

    struct IDTR IDTR = {
        .limit = sizeof(interrupts) - 1, // 0xFFF
        .offset = (uint64_t) &interrupts,
    };
    __asm__("lidt %0" : : "m" (IDTR));

    #define ENABLE_LAPIC (1 << 8)
    lapic_mmoutl(LAPIC_TPR, 0);
    lapic_mmoutl(LAPIC_DFR, 0xffffffff); // flat model
    lapic_mmoutl(LAPIC_LDR, 0x1000000); // all processors have logical APIC ID = 1
    lapic_mmoutl(LAPIC_SVR, ENABLE_LAPIC | INT_SPURIOUS);

    /* disable all I/O APIC entries */
    uint32_t ioapic_version = ioapic_in(IOAPICVER);
    uint32_t entries_count = ((ioapic_version >> 16) & 0xFF) + 1;
    for (uint32_t i = 0; i < entries_count; i++) {
        #define DISABLE_INTERRUPT (1 << 16)
        set_entry(i, DISABLE_INTERRUPT);
    }

    #define IRQ_TIMER 0
    #define IRQ_KEYBOARD 1
    set_entry(remap(IRQ_TIMER), INT_TIMER);
    set_entry(remap(IRQ_KEYBOARD), INT_KEYBOARD);

    /* enable maskable interrupts */
    __asm__("sti");
}
