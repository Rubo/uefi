extern double time_since_boot;
extern uint64_t base_frequency;
extern uint16_t divisor;

void tick(void);
void sleep(uint64_t milliseconds);
void sleepd(double seconds);
