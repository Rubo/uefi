struct point {
    int32_t x, y; // int because ungetc needs to know if they are negative
};

struct renderer {
    struct framebuffer *framebuffer;
    struct PSF1_font psf1;
    struct point cursor;
    uint32_t clear_colour;
};

extern struct renderer renderer;

void putchar(char c);
void ungetc();
void print(char *string);
