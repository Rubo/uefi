#include "common.h"
#include "console.h"

#define clear_screen(framebuffer) memset(framebuffer->address, 0, framebuffer->size)

struct renderer renderer;

void putchar(char c) {
    if (c == '\n' || renderer.cursor.x + 8 >= renderer.framebuffer->width) {
        renderer.cursor.x = 0;
        renderer.cursor.y += 16;
    }
    if (c == '\n' || renderer.cursor.y >= renderer.framebuffer->height) {
        return;
    }

    uint32_t  colour = 0xFFFFFFFF;
    uint32_t *base   = renderer.framebuffer->address;
    char *glyph = &renderer.psf1.glyph_buffer[c * renderer.psf1.header.char_size];
    for (uint32_t y = renderer.cursor.y; y < renderer.cursor.y + 16; y++) {
        for (uint32_t x = renderer.cursor.x; x < renderer.cursor.x + 16; x++) {
            /* select the bit inside the bitmap */
            if ((*glyph & (0b10000000 >> (x - renderer.cursor.x))) > 0) {
                base[x + y * renderer.framebuffer->stride] = colour;
            }
        }
        glyph++;
    }
    renderer.cursor.x += 8;
}

void print(char *string) {
    uint32_t x = 0;
    for (char *c = string; *c; c++) {
        putchar(*c);
    }
}

void ungetc() {
    if (renderer.cursor.x <= 0) {
        if (renderer.cursor.y == 0) {
            return;
        }
        uint32_t columns = (renderer.framebuffer->width - 1) / 8;
        renderer.cursor.x = columns * 8;
        renderer.cursor.y -= 16;
        if (renderer.cursor.y < 0) {
            renderer.cursor.y = 0;
        }
    }

    uint32_t *base = renderer.framebuffer->address;
    for (uint32_t y = renderer.cursor.y; y < renderer.cursor.y + 16; y++) {
        for (uint32_t x = renderer.cursor.x - 8; x < renderer.cursor.x; x++) {
            base[x + y * renderer.framebuffer->stride] = renderer.clear_colour;
        }
    }

    renderer.cursor.x -= 8;
}
